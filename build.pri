SPECPATH = $$split(QMAKESPEC, /)
MYPLATFORM = $$last(SPECPATH)
target.path = $${PWD}/build_qt$${QT_VERSION}_$$MYPLATFORM

message( $$target.path )

createDirs.commands = $(MKDIR) $$target.path
first.depends += createDirs
QMAKE_EXTRA_TARGETS += createDirs

