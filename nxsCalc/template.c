#include "transmission.h"


typedef struct TransmissionData {
  double *wavelength;
  double *transmission;
  unsigned int size = 0;
} TransmissionData;


NXS_UnitCell initializeTransmissionWithParameterFile(TransmissionData *d, const char filename[], double length_cm )
{
  NXS_UnitCell uc;
  unsigned int i=0;
  if( NXS_ERROR_OK==nxs_initFromParameterFile( filename, &uc ) )
  {
    for( i=0; i<d->size; i++ )
      d->transmission[i] = transmission( d->wavelength[i], &uc, length_cm );
  }
  return uc;
}


void iterateTransmission(TransmissionData *d, NXS_UnitCell *uc, double length_cm )
{
  unsigned int i=0;

  // change parameters

  nxs_initHKL( &uc );
  unsigned int i=0;
  if( NXS_ERROR_OK==nxs_initFromParameterFile( filename, &uc ) )
  {
    for( i=0; i<d->size; i++ )
      d->transmission[i] = transmission( d->wavelength[i], &uc, length_cm );
  }
}



int main( int argc, char* argv[] )
{
  const char filename[] = "Fe.nxs";
  TransmissionData d;
  NXS_UnitCell uc = initializeTransmissionWithParameterFile( &d, filename, length_cm );

  // change lattice constants
  uc.a = 2.855;

  // or change C2 parameter
  uc.mph_c2 = 10.0;

  // or change temperature
  uc.temperature = 1000.0;

  // or change density
  uc.density = 7.1;

  iterateTransmission( &d, &uc, length_cm );


  return 0;
}
