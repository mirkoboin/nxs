#include "transmission.h"



double totalcrossection( double lambda, NXS_UnitCell* uc )
{
  return nxs_CoherentElastic(lambda,uc) \
      + nxs_Absorption(lambda,uc) \
      + nxs_TotalInelastic_COMBINED(lambda,uc) \
      + nxs_IncoherentElastic(lambda,uc);
}



double attenuation( double lambda, NXS_UnitCell* uc )
{
  return totalcrossection(lambda,uc) / (uc->mass /  uc->density / 0.60221417930);
}



double transmission( double lambda, NXS_UnitCell* uc, double length )
{
  return exp( -1.0 * attenuation(lambda,uc) * length );
}
