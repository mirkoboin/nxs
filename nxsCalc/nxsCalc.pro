SOURCES += \
    main.c \
    transmission.c \
    template.c

TEMPLATE = app
CONFIG += console
QT -= gui

LIBS += $${PRE_TARGETDEPS}

debug {
  NXSLIBDIR = ../libnxs/Debug/
}
release {
  NXSLIBDIR = ../libnxs/Release/
}

INCLUDEPATH += ../libnxs ./

LIBS += -L$${NXSLIBDIR} -lnxs

HEADERS += \
    transmission.h
