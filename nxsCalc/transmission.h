#ifndef TRANSMISSION_H
#define TRANSMISSION_H

#include <nxs.h>

double totalcrossection( double lambda, NXS_UnitCell* uc );
double attenuation( double lambda, NXS_UnitCell* uc );
double transmission( double lambda, NXS_UnitCell* uc, double length );


#endif // TRANSMISSION_H
