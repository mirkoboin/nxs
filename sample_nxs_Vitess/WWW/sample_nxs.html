<!DOCTYPE doctype PUBLIC "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" type="text/css" href="default.css">
  <link rel="shortcut icon" type="image/x-icon" href="vitess.ico" />
  <title>Module sample_nxs</title>
</head>
  <body>

<h1> VITESS Module sample_nxs</h1>
<h3> Author: Mirko Boin</h3>

The sample_nxs module describes a polycrystalline sample based on its unit cell definition and was originally developed to simulate Bragg edge neutron transmission experiments. The module handles coherent and incoherent scattering, as well as absorption and transmission. The individual probabilities for those events are determined using the calculation of wavelength-dependent neutron cross sections (nxs). These cross sections are computed for each atom contributing to a unit cell, such that complex material compositions can be simulated. The nxs concept is briefly described.

<h3> CONTENT</h3>
<a href="#section1">1. Simulation description</a><br>
&nbsp;&nbsp; <a href="#section1.1">1.1 Unit cell initialisation</a><br>
&nbsp;&nbsp; <a href="#section1.2">1.2 Neutron cross section calculation</a><br>
&nbsp;&nbsp; <a href="#section1.3">1.3 Neutron-sample interactions</a><br>
<a href="#section2">2. Module parameters</a><br>
<a href="#section3">3. Input files</a><br>
&nbsp;&nbsp; <a href="#section3.1">3.1 Sample geometry file</a><br>
&nbsp;&nbsp; <a href="#section3.2">3.2 NXS parameter file</a><br>
<a href="#development">4. Further developments</a><br>
<a href="#references">5. References</a><br>



<h3><a name="section1">1. Simulation description</a></h3>

<h4><a name="section1.1"> 1.1 Unit cell initialisation</a></h4>
Prior to any wavelength-dependent neutron cross section calculation, the crystal properties are initialised at first. Therefore, a unit cell is virtually composed using the existing routines of the SgInfo library [<a href="#grosse1995">Grosse-Kunstleve, 1995</a>]. The permission to use SgInfo is granted by the following copyright and permission notice:<br><br>
<center><table width="60%"><tr><td><em>Space Group Info (c) 1994-96 Ralf W. Grosse-Kunstleve<br><br>
Permission to use and distribute this software and its documentation for noncommercial use and without fee is hereby granted, provided that the above copyright notice appears in all copies and that both that copyright notice and this permission notice appear in the supporting documentation. It is not allowed to sell this software in any way. This software is not in the public domain.</td></tr></table></em></center><br>

The unit cell initialisation requires a number of input parameters:
<UL>
<LI>Space group number (Sch&ouml;nflies and Hermann-Mauguin symbols are also possible),
<LI>Lattice parameters (<em>a, b, c, &alpha;, &beta; and &gamma;</em>),
<LI>Properties of atoms contributing to the unit cell (<em>&sigma;</span><sub>coh</sub></em>, <em>&sigma;</span><sub>inc</sub></em>, <em>&sigma;<sub>abs</sub></em>, <em>M</em> and <em>&theta;<sub>D</sub></em>)
<LI>Arrangment of atoms inside the unit cell (given by the Wyckoff position).
</UL>
Each of these parameters is served by an nxs parameter file (see <a href="#section3.2">§3.2</a> for details). A unit cell is composed and the present lattice reflections and symmetry equivalent lattice planes are determined. As part of the initialisation of the virtual sample, the lattice distances <em>d</em>, structure factors <em>F</em> for all <em>hkl</em> Miller indices and the unit cell volume are calculated to provide the necessary parameters for the neutron cross section calculation.


<h4><a name="section1.2"> 1.2 Neutron cross section calculation</a></h4>
The fraction of neutrons performing different neutron-matter interactions can be quantified
by means of cross sections, given in barns (1 barn=10<sup>-24</sup> cm<sup>2</sup>). Following the descriptions of <a href="#granada1984">Granada [1984]</a> and <a href="#vogel2000">Vogel [2000]</a>, the total neutron cross section is given by:

<center><em>&sigma;<sub>total</sub>(&lambda;) = <span style="text-decoration: overline">&sigma;</span><sub>coh</sub> <font size="+3">(</font> S<sub>coh_el</sub>(&lambda;) + S<sub>coh_inel</sub>(&lambda;) <font size="+3">)</font> + <span style="text-decoration: overline">&sigma;</span><sub>inc</sub> <font size="+3">(</font> S<sub>inc_el</sub>(&lambda;) + S<sub>inc_inel</sub>(&lambda;) <font size="+3">)</font> + &sigma;<sub>abs</sub>(&lambda;)</em></center>
<br><br>
where, <em>&sigma;<sub>abs</sub></em> is the absorption cross section and <em><span style="text-decoration: overline">&sigma;</span></em> describes the average coherent and incoherent scattering contributions, which are computed by the scattering length <em>b</em>. The functions <em>S</em> describe the neutron energy influence as well as the effect of the spatial nuclei arrangement to the elastic and inelastice parts of the cross sections.<br><br>
The structural parameters of the sample material are accounted in the description of the coherent elastic part <em>S<sub>coh_el</sub></em>. Here <em>&sigma;<sub>coh_el</sub>(&lambda;)</em> can be calculated for any type of crystal structure.<br><br>
<center><em>&sigma;<sub>coh_el</sub>(&lambda;) = &lambda;<sup>2</sup> / (2V<sub>0</sub>) <font size="+3">&Sigma;</font> |F<sub>hkl</sub>|<sup>2</sup> d<sub>hkl</sub></em></center>
<br><br>
The summation <em>&Sigma;</em> runs over all sets of lattice planes, indicated by <em>hkl</em>, with a lattice spacing <em>d<sub>hkl</sub></em> smaller than half the size of the selected wavelength <em>&lambda;</em>. <em>V<sub>0</sub></em> is the unit cell volume.<br><br>
The fraction of neutrons that scatter elastically is also influenced by incoherent scattering. The definition of the incoherent elastic component is based on the assumption of thermal motion of the nuclei. Therefore, an isotropic atomic displacement factor <em>B<sub>iso</sub></em> was introduced to include the Debye temperature of an atom.<br><br>
<center><em>S<sub>inc_el</sub>(&lambda;) = &lambda;<sup>2</sup> / (2B<sub>iso,n</sub>) <font size="+3">(</font> 1 - exp( -2B<sub>iso,n</sub> / &lambda;<sup>2</sup>) <font size="+3">)</font></em></center>
<br><br>
For the purpose of a unit cell calculation, the contributions are simply added for each atom <em>n</em> inside the unit cell as they do not depend on the crystal structure.
<br><br>
The inelastic contributions to the neutron cross section calculation describe the probabilities for a neutron to lose or gain energy when interacting with the sample. However, although these probabilities will be determined during the simulation, neutron energy (or wavelength) changes are not considered in the present sample module. Consequently, the author refers to the comprehensive presentations made by <a href="#binder1970">Binder [1970]</a>, <a href="#granada1984">Granada [1984]</a> and <a href="#vogel2000">Vogel [2000]</a> for the descriptions of the inelastic cross sections.
<br><br>
The absorption cross section is determined by the neutron velocity <em>v</em>. Most reference absorption cross section datasets have been measured and stored in terms of a standard neutron velocity of <em>v = 2200 m/s</em>, which is used to compute the absorption cross section for another velocity (and wavelength).
<br><br>
<center><em>&sigma;<sub>abs</sub>(&lambda;) = &sigma;<sub>2200_abs</sub> / 1.798 &Aring; * &lambda;</em></center>
<br><br>
In the following the total and individual neutron cross sections are used to calculate the probabilities for absorption, transmission and scattering of each neutron intersecting with the sample.

<h4><a name="section1.3"> 1.3 Neutron-sample interactions</a></h4>
After the wavelength-dependent neutron cross section is completed a transmission probability for the actual neutron is determined to account for the exponential attenuation:
<br><br>
<center><em>p<sub>transmit</sub> = exp( - &sigma;<sub>total</sub> / V<sub>0</sub> * l)</em></center>
<br><br>
where <em>l</em> is the neutron transmission path length through the sample (i.e. the sample thickness).<br>
The following propagation depends on a command option provided as a module parameter. Here, the 'transmission_only' switch (see <a href="#section2">§2</a>) is used to divide between to different simulation modes. If the option is <em>yes</em>, all neutrons hitting the sample will be transmitted and their weighting factor is attenuated by multiplication with <em>p<sub>transmit</sub></em>. Thus, wavelength-dependent neutron transmission including the effects of the crystallographic structure is simulated. This is useful in order to quickly simulated Bragg edge neutron transmission spectra, for example.<br>
In order realise neutron scattering, the 'transmission_only' switch should be set to <em>no</em> (default). Then, neutron transmission s performed only, if <em>p<sub>transmit</sub></em> > a random number between 0 and 1. Otherwise, another random number (Monte Carlo number) will be created to decide between absorption and coherent and incoherent scattering. The latter is only possible if the module parameter '-I' is activated (see <a href="#section2">§2</a>). For the coherent scattering the available lattice planes, their distances and structure factors are used to determine the Bragg angles and their individual probalities for coherent scattering along the Debye Scherrer cones. The angular range in both directions (Bragg angle and vertical scattering angle) can be restricted to focus on a smaller region as reported in the following section. If it turns out that the neutron is to be absorbed due to the Monte Carlo choice, the neutron is simply not written to the output of the sample_nxs module. 


<h3><a name="section2"> 2. Module parameters</a></h3>

The sample_nxs module povides a number of options to be used from the GUI as well as from the commandline.<br><br>

<table border="1" width="90%" bgcolor=#ddd>
 <tbody>
    <tr>
 <td><b>Parameter</b> <br>
      <b>Unit</b></td>
  <td><b>Description</b></td>
  <td><b>Command option</b></td>
 </tr>
  <tr>
 <td>sample file</td>
  <td>The sample file describes the geometry and properties of the sample.</td>
  <td>&nbsp;-S</td>
 </tr>
  <tr>
 <td>Theta, dTheta,&nbsp; <br>
Phi, dPhi <br>
[deg]</td>
  <td>These parameters describe the solid angle covered by the detector. The
direction (<font face="Symbol">q,f</font>) points to the middle of the covered
area, which extends from [<font face="Symbol">q</font>-<font
 face="Symbol">Dq, q</font>+<font face="Symbol">Dq</font>] and [<font
 face="Symbol">f</font>-<font face="Symbol">Df, f</font>+<font
 face="Symbol">Df</font>].&nbsp; <br>
      <font face="Symbol">q</font> is defined as the angle between +x-axis
(main flight direction of the neutrons) and the Vector R to be described.
      <font face="Symbol">f</font> is the angle between the +y-axis and the
projection of R to the yz-plane. x,y and z form a right-handed system. <br>
&nbsp;!!!Note: If you specify any parameter of <font face="Symbol">Dq, q,
Df, f,</font> you must specify all of them.!!! <br>
&nbsp;The default is a coverage of 4&pi;</td>
  <td>&nbsp;-D, -d, <br>
&nbsp;-P,-p</td>
 </tr>
 <tr>
 <td>repetitions</td>
  <td>'repetitions' specifies the number of data sets (trajectories) generated
for each scattered trajectory. A larger number of repetitions enriches the
population on the detector and gives therefore better statistics in the spectrum.</td>
  <td>&nbsp;-A</td>
 </tr>
 <tr>
 <td>treat all<br>neutrons</td>
  <td><em>yes</em>: treats neutrons not hitting the sample.<br>
	  <em>no</em>: removes neutrons not hitting the sample from the trajectory.</td>
  <td>&nbsp;-a</td>
 </tr>
 <tr>
 <td>transmission<br>only</td>
  <td>'transmission only' specifies the simulation mode:<br>
      <em>yes</em>: only wavelength dependent neutron transmission is handled. Neutrons hitting the sample will be attenuated by I/I<sub>0</sub> = exp(-&mu;(&lambda;)*transmission_length). The neutron flight direction is not changed.<br>
      <em>no</em>: transmission, absorption and coherent and incoherent scattering are simulated.</td>
  <td>&nbsp;-T</td>
 </tr>
 <tr>
 <td>incoherent scattering</td>
  <td><em>yes</em>: neutrons are additionally scattered incoherently.<br>
      <em>no</em>: incoherent scattering is omitted.<br>
	  !!!Note: This option is not considered if 'transmission only' = <em>yes</em>.</td>
  <td>&nbsp;-I</td>
 </tr>

  </tbody>
</table>


<h3><a name="section3"> 3. Input files</a></h3>


<h4><a name="section3.1"> 3.1 Sample geometry file</a></h4>
The sample geometry, placement within the instrument and orientation are summarised by an input file. An example:<BR><BR>
<table border="1"><tr align="center"><td><B>nxs_sample.par</B></td></tr><tr><td><pre>
10.0 0.0 0.0    # sample position relative in [cm] to the coordinate system defined by the preceding module
cub             # sample geometry (cyl, bal or cub)
2.0 1.0	2.3     # thickness or radius, width and height of the sample in [cm]
1.0 0.0 0.0     # orientation of the cylinder (does not need not be normalised)
Fe.nxs          # nxs parameter file to describe the unit cell composition
</pre></td></tr></table><BR>
The sample_nxs module provides three geometry to be simulated (cylinder, sphere (ball), cuboid). Depending on the selected shape, thickness, height and width and/or radius need to be defined. The sample orientation is given by vector components:<br>
<UL>
<LI>Cylinder: the vector is always perpendicular to the top of the cylinder (standard cyl. position (001), height along the z-axis).
<LI>Cuboid: Standard is the (1,0,0) direction, i.e. the sample has a thickness in x-direction, a width in y-direction and height in z-direction. By giving a different vector the whole sample is rotated in this direction, i.e. the planes separated by 'thickness' remain perpendicular to this vector.
<LI>Sphere: no values required. 
</UL>
Another input file (nxs parameter file) is used to define a unit cell. If no input file is assigned or the path the file cannot be found, the sample_nxs module uses a fallback solution. By default an alpha-Fe (bcc) unit cell will be created and used for the simulation. Anything after the # character is interpreted as a comment. 
<p>
The sample geometry file is assigned by the command line parameter -S. This option is mandatory. The VITESS GUI provides input fields to edit the described parameters.
</p>

<h4><a name="section3.2"> 3.2 NXS parameter file</a></h4>
The parameters to define a unit cell are assigned by means of another input file, whose structure is as follows:<BR><BR>
<table border="1"><tr align="center"><td><B>Al.nxs</B></td></tr><tr><td><pre># define the unit cell parameters:
#   space_group                      - the space group number or Hermann or Hall symbol [string]
#   lattice_a, ...b, ...c            - the lattice spacings a,b,c [angstrom]
#   lattice_alpha, ...beta, ...gamma - the lattice angles alpha,beta,gamma [degree]
#   debye_temp                       - the Debye temperature [K]

space_group    = -F 4 2 3 # space group number is also allowed (= 225)
lattice_a = 4.049
lattice_b = 4.049
lattice_c = 4.049
lattice_alpha = 90
lattice_beta = 90
lattice_gamma = 90
debye_temp = 429.0

# add atoms to the unit cell:
# notation is "atom_number = name b_coh sigma_inc sigma_abs_2200 molar_mass x y z"
#   name           - labels the current atom/isotope  [string]
#   b_coh          - the coherent scattering length [fm]
#   sigma_inc      - the incoherent scattering cross section [barns]
#   sigma_abs_2200 - the absorption cross sect. at 2200 m/s [barns]
#   molar_mass     - the Molar mass [g/mol]
#   debye_temp     - the Debye temperature [K]
#   x y z          - the Wyckoff postion of the atom inside the unit cell

[atoms]
add_atom = Al 3.449 0.008 0.23 26.98 0.0 0.0 0.0
</pre></td></tr></table><BR>

<h3><a name="development">4. Further developments</a></h3>
The first official sample_nxs module was release with Vitess 2.11. This sample_nxs version 1.1 was created to take advantage of an updated nxs library. Besides the implementation into Vitess, a sample module for McStas has been realised and is available from the <a href="http://www.mcstas.org/">McStas repository</a>. Furthermore, a <a href="http://geant4.cern.ch/">Geant4</a> module, called <a href="http://nxsg4.web.cern.ch/nxsg4/">NXSG4</a>, for proper simulations of thermal and cold neutrons through crystalline matter was developed. Last but not least, a number of users have successfully applied the nxs routines for their research activities. The list below references publications about the development and applications of nxs: <BR><BR>
<UL>
<LI><a name="boin2011">M. Boin, A. Hilger, N. Kardjilov, S.Y. Zhang, E.C. Oliver, J.A. James, C. Randau &amp; R.C. Wimpory (2011)</a>, <em>J. Appl. Cryst.</em> <b>44</b>(5), 1040-1046, <a href="http://dx.doi.org/10.1107/S0021889811025970">doi:10.1107/S0021889811025970</a>
<LI><a name="strobl2011">Strobl, M.; Hilger, A.; Boin, M.; Kardjilov, N.; Wimpory, R.; Clemens, D.; M�hlbauer, M.; Schillinger, B.; Wilpert, T.; Schulz, C.; Rolfs, K.; Davies, C. M.; O'Dowd, N.; Tiernan, P. & Manke, I. (2011)</a>, <em>Nucl. Instrum. Methods Phys. Res., Sect. A</em> <b>651</b>, 149-155, <a href="http://dx.doi.org/10.1016/j.nima.2010.12.121">doi:10.1016/j.nima.2010.12.121</a>
<LI><a name="boin2012a">M. Boin (2012)</a>, <em>J. Appl. Cryst.</em> <b>45</b>, 603-607, <a href="http://dx.doi.org/10.1107/S0021889812016056">doi:10.1107/S0021889812016056</a>
<LI><a name="boin2012b">M. Boin, R.C. Wimpory, A. Hilger, N. Kardjilov, S.Y. Zhang, M. Strobl (2012)</a>, <em>J. Phys.: Conf. Ser.</em> <b>340</b>, 012022, <a href="http://dx.doi.org/10.1088/1742-6596/340/1/012022">doi:10.1088/1742-6596/340/1/012022</a>
<LI><a name="kittelmann2014">Kittelmann, T.; Stefanescu, I.; Kanaki, K.; Boin, M.; Hall-Wilton, R. & Zeitelhack, K. (2014)</a>, <em>J. Phys.: Conf. Ser.</em> <b>513</b>, 022017, <a href="http://dx.doi.org/10.1088/1742-6596/513/2/022017">doi:10.1088/1742-6596/513/2/022017</a>
<LI><a name="peetermans2014">Peetermans, S.; Tamaki, M.; Hartmann, S.; Kaestner, A.; Morgano, M. & Lehmann, E. H. (2014)</a>, <em>Nucl. Instrum. Methods Phys. Res., Sect. A</em> <b>757</b>, 28-32, <a href="http://dx.doi.org/10.1016/j.nima.2014.04.033">doi:10.1016/j.nima.2014.04.033</a>
</UL>

It is further planned to continue the development of the Vitess module to provide additional capabilities such as strain, texture and multiple scattering as it has been done for McStas [e.g. <a href="#boin2011">Boin et al., 2011</a>]. More up-to-date information is posted on <a href="http://www.helmholtz-berlin.de/people/boin/nxs_en.html">http://www.helmholtz-berlin.de/people/boin/nxs_en.html</a>.


<h3><a name="references">5. References</a></h3>
<UL>
<LI><a name="binder1970">K. Binder (1970)</a>, <em>Phys. Stat. Sol. B</em> <b>41</b>(5), 767-779, <a href="http://dx.doi.org/10.1002/pssb.19700410233">doi:10.1002/pssb.19700410233</a>
<LI><a name="granada1984">J.R. Granada (1984)</a>, <em>Z. Naturforsch. Teil A</em> <b>39</b>, 1160-�1167.
<LI><a name="grosse1995"> R.W. Grosse-Kunstleve (1995)</a>, <em>SgInfo - a Comprehensive Collection of ANSI C Routines for the Handling of Space Groups</em>, <a href="http://cci.lbl.gov/sginfo/">http://cci.lbl.gov/sginfo/</a>
<LI><a name="vogel2000">S.C. Vogel (2000)</a>, PhD thesis, Christian-Albrecht University, Kiel, Germany.
<LI><a name="freund1983">A.K. Freund (1983)</a>, <em>Nucl. Instr. Meth.</em> <b>213</b>, 495-501.


</UL>
<hr>
<a href="vitess.html">Back to VITESS overview</a><br>
<a href="mailto:vitess@helmholtz-berlin.de">vitess@helmholtz-berlin.de</a>

</body>
</html>
