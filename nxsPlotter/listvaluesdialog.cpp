
#include "listvaluesdialog.h"
#include "QFileDialog"
#include "QTextStream"

ListValuesDialog::ListValuesDialog(QWidget* parent, Qt::WindowFlags fl)
: QDialog(parent, fl), Ui::ListValuesDialog()
{
  setupUi(this);
  connect( pushButton_save, SIGNAL(clicked()), this, SLOT(saveList()) );

}



ListValuesDialog::~ListValuesDialog()
{
}




/*!
    \fn ListValuesDialog::setValuesText()
 */
void ListValuesDialog::setValuesText( QString text )
{
  textEdit_values->setText( text );
}




/*!
    \fn ListValuesDialog::saveList()
 */
void ListValuesDialog::saveList()
{
  QString fileName = QFileDialog::getSaveFileName( this, tr("Save File") );
  if ( fileName.isEmpty() )
    return;

  QFile file( fileName );
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    return;

  QTextStream out( &file );
    out << textEdit_values->toPlainText();

  file.close();
}
