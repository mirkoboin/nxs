/*-------------------------------------------------*/
/* created by Mirko Boin                           */
/*            Helmholtz-Zentrum Berlin (HZB)       */
/*            boin@helmholtz-berlin.de             */
/*-------------------------------------------------*/
#ifndef INPUTFILEDIALOG_H
#define INPUTFILEDIALOG_H

#include <QDialog>
#include "ui_inputfiledialog.h"

/**
  @author Mirko Boin, Helmholtz Centre Berlin for Materials and Energy <boin@helmholtz-berlin.de>
*/

/*! 
 *  \brief Dialog to set up the input file containing example parameters.
 *
 */
class InputFileDialog : public QDialog, private Ui::InputFileDialog
{
  Q_OBJECT

  public:
    InputFileDialog(QWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~InputFileDialog();
    void setFileName( QString );
    QString getFileName();
    /*$PUBLIC_FUNCTIONS$*/

  public slots:
    /*$PUBLIC_SLOTS$*/

  protected:
    /*$PROTECTED_FUNCTIONS$*/

  protected slots:
    /*$PROTECTED_SLOTS$*/

  private slots:
    void on_pushButton_open_clicked();
};

#endif

