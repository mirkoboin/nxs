/*-------------------------------------------------*/
/* created by Mirko Boin                           */
/*            Helmholtz-Zentrum Berlin (HZB)       */
/*            boin@helmholtz-berlin.de             */
/*-------------------------------------------------*/
#ifndef PLOTCOLORSDIALOG_H
#define PLOTCOLORSDIALOG_H

#include <QDialog>
#include "ui_plotcolorsdialog.h"


/**
  @author Mirko Boin, Helmholtz Centre Berlin for Materials and Energy <boin@helmholtz-berlin.de>
*/

/*! 
 *  \brief Dialog to set up the plot colors.
 *
 */
class PlotColorsDialog : public QDialog, private Ui::PlotColorsDialog
{
  Q_OBJECT

  public:
    PlotColorsDialog(QWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~PlotColorsDialog();
    void setColors( QColor, QColor, QColor, QColor, QColor, QColor );
    QColor color_coh_el();
    QColor color_coh_inel();
    QColor color_inc_el();
    QColor color_inc_inel();
    QColor color_absorption();
    QColor color_total();
    /*$PUBLIC_FUNCTIONS$*/


  public slots:
    /*$PUBLIC_SLOTS$*/

  protected:
    /*$PROTECTED_FUNCTIONS$*/

  protected slots:
    /*$PROTECTED_SLOTS$*/

};

#endif

