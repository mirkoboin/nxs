/*-------------------------------------------------*/
/* created by Mirko Boin                           */
/*            Helmholtz-Zentrum Berlin (HZB)       */
/*            boin@helmholtz-berlin.de             */
/*-------------------------------------------------*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include <QSettings>
#include <QMenu>
#include <QThread>

#include "ui_mainwindow.h"
#include "braggedgecalculator.h"


/**
  @author Mirko Boin, Helmholtz Centre Berlin for Materials and Energy <boin@helmholtz-berlin.de>
*/






/*! 
 *  \brief Main window.
 *         This class represents the main window of this application.
 *
 *  Detailed description starts here.
 */
class MainWindow : public QMainWindow, private Ui::MainWindow
{
  Q_OBJECT

  public:
    MainWindow(QWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);

  private slots:
    void plotCrosssections();
    void refreshForm( QString );
    void disableLatticeBox( QString );
    void toggleLegend(bool show);
    void savePlotValues();
    void listPlotValues();
    void listHKLValues();
    void changeStyle( QAction* );
    void on_action_number_precision_triggered();
    void changePlotColors();
    void changeInputFile();
    void writeSamplesFile();
    void deleteSample();
    void about();
    void on_pushButton_plus_clicked();
    void on_pushButton_minus_clicked();
    void on_pushButton_listPositions_clicked();
    void changeSpaceGroupNoIndex( int index );
    void changeSpaceGroupHermannIndex( int index );
    void changeSpaceGroupHallIndex( int index );
    void on_tableWidget_selection_currentItemChanged( QTableWidgetItem * current, QTableWidgetItem * previous );
    void on_action_showCrossSections_triggered( bool checkState );
    void on_action_showAttenuationCoeff_triggered( bool checkState );
    void on_action_showTransmission_triggered( bool checkState );
    void on_pushButton_plusTexture_clicked();
    void on_pushButton_minusTexture_clicked();
    void on_checkBox_enableTexture_stateChanged( int state );
    void mousePress();
    void mouseWheel();
    void contextMenuRequest(QPoint pos);
    void onThreadFinished();
    void onXAxisUnitTriggered( QAction *action );
    void onXAxisScalingTriggered( QAction *action );
    void onYAxisScalingTriggered( QAction *action );


  private:
    BraggEdgeCalculator calc;           /*!< A BraggEdgeCalculator object for cross section calculation */
    QHBoxLayout *diagramLayout;
    unsigned int numberPrecision;
    QDir configPath;
    QString inputFileName;
    std::vector<NXS_Texture> textureVector;
    QMenu *menuXAxis;
    QMenu *menuYAxis;
    QActionGroup *xAxisScaling;
    QAction *xAxisLinear;
    QAction *xAxisLog10;
    QActionGroup *yAxisScaling;
    QAction *yAxisLinear;
    QAction *yAxisLog10;
    QActionGroup *xAxisUnit;
    QAction *xAxisWavelength;
    QAction *xAxisEnergy;
    QAction *showLegend;
    QAction *listPlot;
    QAction *listHKL;
    QAction *savePlot;
    unsigned int nLambda;
    double lambdaBegin;
    double lambdaEnd;
    unsigned int max_hkl;
    QThread thread;
    QDir _settingsPath;
    QSettings* _settings;
    double _programversion;

  private:
    void changeNumberPrecision();
    void setSampleProperties();
    void readSamplesFile();
    void initSpaceGroups();
    void setupApplicationSettings();
    void restoreApplicationSettings();
};

#endif

