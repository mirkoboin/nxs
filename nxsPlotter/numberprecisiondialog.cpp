


#include "numberprecisiondialog.h"


NumberPrecisionDialog::NumberPrecisionDialog(QWidget* parent, Qt::WindowFlags fl)
: QDialog( parent, fl ), Ui::NumberPrecisionDialog()
{
  setupUi(this);
  connect( spinBox_decimalPlaces, SIGNAL(valueChanged(int)), this, SLOT(onDecimalPlacesValueChanged(int)) );
}


NumberPrecisionDialog::~NumberPrecisionDialog()
{
}

/*$SPECIALIZATION$*/


/*!
    \fn NumberPrecisionDialog::setNumberPrecision( unsigned int )
 */
void NumberPrecisionDialog::setNumberPrecision(int prec)
{
  spinBox_decimalPlaces->setValue( prec );
  doubleSpinBox_startWavelength->setDecimals( prec );
  doubleSpinBox_stopWavelength->setDecimals( prec );
}

double NumberPrecisionDialog::getStartWavelength()
{
  return doubleSpinBox_startWavelength->value();
}

void NumberPrecisionDialog::setStartWavelength(double lambda)
{
  doubleSpinBox_startWavelength->setValue( lambda );
}

double NumberPrecisionDialog::getStopWavelength()
{
  return doubleSpinBox_stopWavelength->value();
}

void NumberPrecisionDialog::setStopWavelength(double lambda)
{
  doubleSpinBox_stopWavelength->setValue( lambda );
}

unsigned int NumberPrecisionDialog::getSamplingPoints()
{
  return unsigned( spinBox_samplingpoints->value() );
}

void NumberPrecisionDialog::setSamplingPoints(unsigned int points)
{
  spinBox_samplingpoints->setValue( int(points) );
}

unsigned int NumberPrecisionDialog::getMaxHKL()
{
  return unsigned( spinBox_maxHKL->value() );
}

void NumberPrecisionDialog::setMaxHKL(unsigned int maxHKL)
{
  spinBox_maxHKL->setValue( int(maxHKL) );
}

void NumberPrecisionDialog::onDecimalPlacesValueChanged(int prec)
{
  doubleSpinBox_startWavelength->setDecimals( prec );
  doubleSpinBox_stopWavelength->setDecimals( prec );
}



/*!
    \fn NumberPrecisionDialog::getNumberPrecision()
 */
unsigned int NumberPrecisionDialog::getNumberPrecision()
{
  return spinBox_decimalPlaces->value();
}
