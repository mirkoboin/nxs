#ifdef _MSC_VER
  #define _USE_MATH_DEFINES
#endif

#include <math.h>
#include "braggedgecalculator.h"
#include <QThread>

/**
 * Constructor
 */
BraggEdgeCalculator::BraggEdgeCalculator() : QObject()
{
  nLambda = 0;
  _samplePacking = 1.0;
  _sampleThickness = 1.0;
  _sampleTemperature = 293.0;
}


/**
 * Destructor
 */
BraggEdgeCalculator::~BraggEdgeCalculator()
{
}



/*!
    \fn BraggEdgeCalculator::calculateCrosssections
 */
void BraggEdgeCalculator::calculateCrosssections()
{
  vdXsectCohEl.resize( nLambda );
  vdXsectCohInel.resize( nLambda );
  vdXsectIncEl.resize( nLambda );
  vdXsectIncInel.resize( nLambda );
  vdXsectAbs.resize( nLambda );
  vdXsectTotal.resize( nLambda );
  vdXsectSPh.resize( nLambda );
  vdXsectMPh.resize( nLambda );
  vdAttenCoeff.resize( nLambda );
  vdTransmission.resize( nLambda );

  NXS_MarchDollase marchDollase;
  if( textureVector->size() )
  {
    marchDollase = nxs_initMarchDollase( &unitcell );
    for( unsigned int i=0; i<textureVector->size(); i++ )
      nxs_addTexture( &marchDollase, textureVector->at(i) );
  }

  // calculate neutron cross sections depending on lambda */
  for( unsigned int i=0; i<nLambda; i++ )
  {
    /* calculate all data for each wavelength... xsect = cross section */
    if( textureVector->size() )
      vdXsectCohEl[i] = nxs_CoherentElasticTexture( vdLambda[i], &marchDollase );
    else
      vdXsectCohEl[i] = nxs_CoherentElastic( vdLambda[i], &unitcell );

    vdXsectAbs[i]     = nxs_Absorption( vdLambda[i], &unitcell );
    vdXsectCohInel[i] = nxs_CoherentInelastic( vdLambda[i], &unitcell );
    vdXsectIncEl[i]   = nxs_IncoherentElastic( vdLambda[i], &unitcell );
    vdXsectIncInel[i] = nxs_IncoherentInelastic( vdLambda[i], &unitcell );

    vdXsectSPh[i] = nxs_SinglePhonon( vdLambda[i], &unitcell );
    vdXsectMPh[i] = nxs_MultiPhonon( vdLambda[i], &unitcell );

    vdXsectTotal[i] = vdXsectCohEl[i] + vdXsectAbs[i] + vdXsectSPh[i] + vdXsectIncEl[i] + vdXsectMPh[i];
    vdAttenCoeff[i] = vdXsectTotal[i] / unitcell.volume;

    vdTransmission[i] = exp( -vdAttenCoeff[i]*_sampleThickness*_samplePacking );
  }

  this->thread()->quit();
}




/*!
    \fn BraggEdgeCalculator::setMarchDollaseTexture( std::vector<Texture> *tv )
 */
void BraggEdgeCalculator::setMarchDollaseTexture(std::vector<NXS_Texture> *tv )
{
  textureVector = tv;
}

void BraggEdgeCalculator::setCalculationRange(double startLambda, double endLambda, unsigned int samplingPoints)
{
  nLambda = samplingPoints;
  double step_width = fabs(endLambda - startLambda)/nLambda;
  if( endLambda<startLambda )
    startLambda = endLambda;

  vdLambda.resize( nLambda );

  for( unsigned int i=0; i<nLambda; i++ )
    vdLambda[i] = step_width*double(i)+startLambda;
}


void BraggEdgeCalculator::setSampleProperties(double packingdensity, double thickness_cm, double temperature)
{
  _samplePacking = packingdensity;
  _sampleThickness = thickness_cm;
  _sampleTemperature = temperature;

}
