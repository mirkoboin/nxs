
#include "mainwindow.h"

#include <QStyleFactory>
#include <QFileInfo>
#include <QMessageBox>
#include <QTextStream>
#include <QFileDialog>
#include <QSettings>
#include <QDate>
#include <QHeaderView>
#include <QVector>

#include "aboutdialog.h"
#include "listvaluesdialog.h"
#include "numberprecisiondialog.h"
#include "plotcolorsdialog.h"
#include "inputfiledialog.h"
#include "spinboxdelegate.h"
#include "doublespinboxdelegate.h"

#include <QDebug>


/**
 * The Constructor initialises the GUI and connects signals and slots.
 * Default colors will be set as well as a settings file containing saved examples. If examples are found, they will be loaded in the QComboBox.
 * @param parent 
 * @param fl 
 */
MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags fl)
: QMainWindow( parent, fl ), Ui::MainWindow()
{
  setupUi(this);
  setupApplicationSettings();

  numberPrecision = 3;
  unsigned int maxHKL = 15;
  pushButton_listPositions->setEnabled( false );

  connect( actionAbout_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()) );
  connect( actionAbout, SIGNAL(triggered()), this, SLOT(about()) );

  // input selection table
  tableWidget_selection->verticalHeader()->hide();
  tableWidget_selection->horizontalHeader()->hide();
  tableWidget_selection->horizontalHeader()->setStretchLastSection( true );
  tableWidget_selection->setEditTriggers( QAbstractItemView::NoEditTriggers );
  tableWidget_selection->setCurrentCell( 0, 0 );

  // table for atomic properties
  tableWidget_atoms->verticalHeader()->hide();
  tableWidget_atoms->horizontalHeader()->show();
  tableWidget_atoms->horizontalHeader()->setHighlightSections( false );
  tableWidget_atoms->setItemDelegateForColumn( 1, new DoubleSpinBoxDelegate( numberPrecision ) );
  tableWidget_atoms->setItemDelegateForColumn( 2, new DoubleSpinBoxDelegate( numberPrecision ) );
  tableWidget_atoms->setItemDelegateForColumn( 3, new DoubleSpinBoxDelegate( numberPrecision ) );
  tableWidget_atoms->setItemDelegateForColumn( 4, new DoubleSpinBoxDelegate( numberPrecision ) );
  tableWidget_atoms->setItemDelegateForColumn( 5, new DoubleSpinBoxDelegate( numberPrecision ) );
  tableWidget_atoms->setItemDelegateForColumn( 6, new DoubleSpinBoxDelegate( numberPrecision ) );
  tableWidget_atoms->setItemDelegateForColumn( 7, new DoubleSpinBoxDelegate( numberPrecision ) );
  tableWidget_atoms->setItemDelegateForColumn( 8, new DoubleSpinBoxDelegate( numberPrecision ) );

  tableWidget_atoms->horizontalHeaderItem(0)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">Atom name</body></html>" );
  tableWidget_atoms->horizontalHeaderItem(1)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">b<sub>coherent</sub> [fm]<br>coherent scattering length</body></html>" );
  tableWidget_atoms->horizontalHeaderItem(2)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">&sigma;<sub>inc</sub> [barns]<br>incoherent scattering cross section</body></html>" );
  tableWidget_atoms->horizontalHeaderItem(3)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">&sigma;<sub>abs</sub> [barns]<br>absorption cross section</body></html>" );
  tableWidget_atoms->horizontalHeaderItem(4)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">Molar mass [u]</body></html>" );
  tableWidget_atoms->horizontalHeaderItem(5)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">Site occupation factor</body></html>" );
  tableWidget_atoms->horizontalHeaderItem(6)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">x-positon<br>Wyckoff position</body></html>" );
  tableWidget_atoms->horizontalHeaderItem(7)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">y-positon<br>Wyckoff position</body></html>" );
  tableWidget_atoms->horizontalHeaderItem(8)->setToolTip( "<html><body style=\"font-family: 'Helvetica [Adobe]'; font-size:11pt;\">z-positon<br>Wyckoff position</body></html>" );

  // table for texture properties
  tableWidget_texture->verticalHeader()->hide();
  tableWidget_texture->horizontalHeader()->show();
  tableWidget_texture->horizontalHeader()->setHighlightSections( false );
  tableWidget_texture->setItemDelegateForColumn( 0, new SpinBoxDelegate( maxHKL ) );
  tableWidget_texture->setItemDelegateForColumn( 1, new SpinBoxDelegate( maxHKL ) );
  tableWidget_texture->setItemDelegateForColumn( 2, new SpinBoxDelegate( maxHKL ) );
  tableWidget_texture->setItemDelegateForColumn( 3, new DoubleSpinBoxDelegate( numberPrecision ) );
  tableWidget_texture->setItemDelegateForColumn( 4, new DoubleSpinBoxDelegate( numberPrecision ) );

  initSpaceGroups();

  QActionGroup *actGroup = new QActionGroup(this);
  actGroup->addAction( action_showAttenuationCoeff );
  actGroup->addAction( action_showCrossSections );
  actGroup->addAction( action_showTransmission );
  actGroup->setExclusive( true );

  widget_plot_attenuation->xAxis->setLabel( "neutron wavelength [" + QString(QChar(0xC5)) + "]" );
  widget_plot_crosssections->xAxis->setLabel( "neutron wavelength [" + QString(QChar(0xC5)) + "]" );
  widget_plot_transmission->xAxis->setLabel( "neutron wavelength [" + QString(QChar(0xC5)) + "]" );
  widget_plot_attenuation->yAxis->setLabel("attenuation coefficient [1/cm]");
  widget_plot_crosssections->yAxis->setLabel("cross section [barns]");
  widget_plot_transmission->yAxis->setLabel("transmission [I/I0]");
  widget_plot_crosssections->xAxis->setRange(0, 10);
  widget_plot_crosssections->yAxis->setRange(0, 50);
  widget_plot_crosssections->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend );
  widget_plot_attenuation->xAxis->setRange(0, 10);
  widget_plot_attenuation->yAxis->setRange(0, 1);
  widget_plot_attenuation->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend );
  widget_plot_transmission->xAxis->setRange(0, 10);
  widget_plot_transmission->yAxis->setRange(0, 1);
  widget_plot_transmission->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend );

  for( int i=0; i<8; i++ )
  {
    widget_plot_crosssections->addGraph();
    widget_plot_crosssections->graph(i)->setLineStyle( QCPGraph::lsLine );
    widget_plot_crosssections->graph(i)->setScatterStyle( QCPScatterStyle::ssNone );
  }
  widget_plot_crosssections->graph(0)->setPen( QPen(QColor("#00ff00")) );
  widget_plot_crosssections->graph(0)->setName( "coherent elastic" );
  widget_plot_crosssections->graph(1)->setPen( QPen(QColor("#0000ff")) );
  widget_plot_crosssections->graph(1)->setName( "coherent inelastic" );
  widget_plot_crosssections->graph(2)->setPen( QPen(QColor("#00ffff")) );
  widget_plot_crosssections->graph(2)->setName( "incoherent elastic" );
  widget_plot_crosssections->graph(3)->setPen( QPen(QColor("#ff00ff")) );
  widget_plot_crosssections->graph(3)->setName( "incoherent inelastic" );
  widget_plot_crosssections->graph(4)->setPen( QPen(QColor("#ff0000")) );
  widget_plot_crosssections->graph(4)->setName( "absorption" );
  widget_plot_crosssections->graph(5)->setPen( QPen(QColor("#000000")) );
  widget_plot_crosssections->graph(5)->setName( "total" );
  widget_plot_crosssections->graph(6)->setPen( QPen(QColor("#00BBBB")) );
  widget_plot_crosssections->graph(6)->setName( "sph" );
  widget_plot_crosssections->graph(7)->setPen( QPen(QColor("#CCCC00")) );
  widget_plot_crosssections->graph(7)->setName( "mph" );

  widget_plot_attenuation->addGraph();
  widget_plot_attenuation->graph(0)->setLineStyle( QCPGraph::lsLine );
  widget_plot_attenuation->graph(0)->setScatterStyle( QCPScatterStyle::ssNone );
  widget_plot_attenuation->graph(0)->setPen( QPen(QColor("#000000")) );
  widget_plot_attenuation->graph(0)->setName( "attenuation coeff." );

  widget_plot_transmission->addGraph();
  widget_plot_transmission->graph(0)->setLineStyle( QCPGraph::lsLine );
  widget_plot_transmission->graph(0)->setScatterStyle( QCPScatterStyle::ssNone );
  widget_plot_transmission->graph(0)->setPen( QPen(QColor("#000000")) );
  widget_plot_transmission->graph(0)->setName( "transmission" );


  // add menu for copying and saving the plot
  showLegend = new QAction( "&Show legen&d", this );
  showLegend->setShortcut( QKeySequence("Ctrl+D") );
  showLegend->setStatusTip( "Show plot legend" );
  showLegend->setCheckable( true );
  showLegend->setChecked( true );
  listPlot = new QAction( "&List Plot", this );
  listPlot->setShortcut( QKeySequence("Ctrl+L") );
  listPlot->setStatusTip( "Show all plot values in a list" );
  savePlot = new QAction( "&Save Plot", this );
  savePlot->setShortcut( QKeySequence("Ctrl+S") );
  savePlot->setStatusTip( "Save all plot values in a text file" );
  listHKL = new QAction( "List &hkl", this );
  listHKL->setShortcut( QKeySequence("Ctrl+H") );
  listHKL->setStatusTip( "Show all hkl reflections in a list" );

  menuXAxis = new QMenu( "X axis", plotMenu );
  xAxisUnit = new QActionGroup( menuXAxis );
  xAxisWavelength = new QAction( "Neutron wavelength [" + QString(QChar(0xC5)) + "]", xAxisUnit);
  xAxisWavelength->setStatusTip( "Show neutron wavelength on X axis" );
  xAxisWavelength->setCheckable( true );
  xAxisWavelength->setChecked( true );
  xAxisEnergy = new QAction( "Neutron energy [eV]", xAxisUnit );
  xAxisEnergy->setStatusTip( "Show neutron energy on X axis" );
  xAxisEnergy->setCheckable( true );
  xAxisEnergy->setChecked( false );
  xAxisUnit->setExclusive( true );
  menuXAxis->addActions( xAxisUnit->actions() );
  connect( xAxisUnit, SIGNAL(triggered(QAction*)), this, SLOT(onXAxisUnitTriggered(QAction*)) );

  xAxisScaling = new QActionGroup( menuXAxis );
  xAxisLinear = new QAction( "Linear", xAxisScaling );
  xAxisLinear->setStatusTip( "Turn X axis into linear scale." );
  xAxisLinear->setCheckable( true );
  xAxisLinear->setChecked( true );
  xAxisLog10 = new QAction( "Log10", xAxisScaling );
  xAxisLog10->setStatusTip( "Turn X axis into log10 scale." );
  xAxisLog10->setCheckable( true );
  xAxisLog10->setChecked( false );
  xAxisScaling->setExclusive( true );
  menuXAxis->addSeparator();
  menuXAxis->addActions( xAxisScaling->actions() );
  connect( xAxisScaling, SIGNAL(triggered(QAction*)), this, SLOT(onXAxisScalingTriggered(QAction*)) );

  plotMenu->addMenu( menuXAxis );

  menuYAxis = new QMenu( "Y axis", plotMenu );
  yAxisScaling = new QActionGroup( menuYAxis );
  yAxisLinear = new QAction( "Linear", yAxisScaling );
  yAxisLinear->setStatusTip( "Turn Y axis into linear scale." );
  yAxisLinear->setCheckable( true );
  yAxisLinear->setChecked( true );
  yAxisLog10 = new QAction( "Log10", yAxisScaling);
  yAxisLog10->setStatusTip( "Turn Y axis into log10 scale." );
  yAxisLog10->setCheckable( true );
  yAxisLog10->setChecked( false );
  yAxisScaling->setExclusive( true );
  menuYAxis->addActions( yAxisScaling->actions() );
  connect( yAxisScaling, SIGNAL(triggered(QAction*)), this, SLOT(onYAxisScalingTriggered(QAction*)) );

  plotMenu->addMenu( menuYAxis );

  plotMenu->addAction( showLegend );
  plotMenu->addSeparator();
  plotMenu->addAction( listPlot );
  plotMenu->addAction( savePlot );
  plotMenu->addAction( listHKL );
  connect( showLegend, SIGNAL(toggled(bool)), this, SLOT(toggleLegend(bool)) );
  connect( listPlot, SIGNAL(triggered()), this, SLOT(listPlotValues()) );
  connect( savePlot, SIGNAL(triggered()), this, SLOT(savePlotValues()) );
  connect( listHKL, SIGNAL(triggered()), this, SLOT(listHKLValues()) );

  connect( toolButton_plot, SIGNAL(clicked()), this, SLOT(plotCrosssections()) );
  connect( comboBox_entries, SIGNAL(currentIndexChanged(QString)), this, SLOT(refreshForm(QString)) );
  connect( comboBox_spaceGroupNo, SIGNAL(currentIndexChanged(QString)), this, SLOT( disableLatticeBox(QString)) );
  connect( pushButtonDelete, SIGNAL(clicked()), this, SLOT(deleteSample()) );
  connect( pushButtonSave, SIGNAL(clicked()), this, SLOT(writeSamplesFile()) );

  // connect slots that takes care that when an axis is selected, only that direction can be dragged and zoomed:
  connect( widget_plot_crosssections, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()) );
  connect( widget_plot_attenuation, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()) );
  connect( widget_plot_transmission, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()) );
  connect( widget_plot_crosssections, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()) );
  connect( widget_plot_attenuation, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()) );
  connect( widget_plot_transmission, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()) );

  // setup policy and connect slot for context menu popup:
  widget_plot_crosssections->setContextMenuPolicy(Qt::CustomContextMenu);
  widget_plot_attenuation->setContextMenuPolicy(Qt::CustomContextMenu);
  widget_plot_transmission->setContextMenuPolicy(Qt::CustomContextMenu);
  connect( widget_plot_crosssections, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)) );
  connect( widget_plot_attenuation, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)) );
  connect( widget_plot_transmission, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)) );

  // style options
  QStringList styleList = QStyleFactory::keys();
  for ( QStringList::Iterator it = styleList.begin(); it != styleList.end(); it++ )
  {
    QAction* styleEntry = new QAction( *it, this );
    menu_style->addAction( styleEntry );
  }
  connect( menu_style, SIGNAL(triggered(QAction*)), this, SLOT(changeStyle(QAction*)) );
  connect( action_plot_colors, SIGNAL(triggered()), this, SLOT(changePlotColors()) );
  connect( action_input_file, SIGNAL(triggered()), this, SLOT(changeInputFile()) );

  calc.moveToThread( &thread );
  connect( &thread, SIGNAL(started()), &calc, SLOT(calculateCrosssections()) );
  connect( &thread, SIGNAL(finished()), this, SLOT(onThreadFinished()) );

  nLambda = 300;
  lambdaBegin = 0.0;
  lambdaEnd = 8.0;
  max_hkl = 8;

  // this is our input file
  QFileInfo fileInfo( _settingsPath,"examples.txt" );
  if ( fileInfo.exists() )
  {
    inputFileName = fileInfo.filePath();
    readSamplesFile();
  }
  else
  {
    //firstUse();
  }

  disableLatticeBox( comboBox_spaceGroupNo->currentText() );

  // TODO: implement Settings for all program parameters
  restoreApplicationSettings();
}


/**
 * Destructor
 */
MainWindow::~MainWindow()
{
}


void MainWindow::setupApplicationSettings()
{
  /* find the settings file of the scanControl program */
#if defined(Q_OS_WIN)
#ifdef _MSC_VER
  char* buf = 0;
  size_t sz = 0;
  if( _dupenv_s(&buf, &sz, "APPDATA") == 0 )
  {
    _settingsPath = QDir( QString::fromLocal8Bit(buf) );
    free( buf );
  }
#else
  _settingsPath = QDir( QString::fromLocal8Bit(getenv("APPDATA")) );
#endif // _MSC_VER
  if( !_settingsPath.exists("nxsPlotter") )
    _settingsPath.mkdir( "nxsPlotter" );
  _settingsPath.cd( "nxsPlotter" );
#else
  _settingsPath = QDir::home();
  if( !_settingsPath.exists(".nxsPlotter") )
    _settingsPath.mkdir( ".nxsPlotter" );
  _settingsPath.cd( ".nxsPlotter" );
#endif // Q_OS_WIN
  QFileInfo fi( _settingsPath, "settings.conf" );
  /* QSettings creates the file on the very first program start */
  _settings = new QSettings( fi.absoluteFilePath(), QSettings::IniFormat, this );

 _programversion = 0.0;
#if defined(VER)
 QString version(VER);
 if( !version.isEmpty() )
   _programversion = version.toDouble();
#endif
}



void MainWindow::restoreApplicationSettings()
{
  /* restore the application version */
  if( 1 /*fabs(_settings->value("nxsPlotter/version", 0 ).toFloat()-_programversion) < 1E-6*/ )
  {
    /* restore the application geometry */
    this->move( _settings->value("window/position", QPoint(200, 200)).toPoint() );
    this->resize( _settings->value("window/size", QSize(600, 600)).toSize() );
    if( _settings->value( "window/fullscreen", false ).toBool() )
      this->setWindowState( Qt::WindowMaximized );

    this->restoreState( _settings->value( "window/state" ).toByteArray() );
    this->splitter->restoreState( _settings->value( "window/splitter" ).toByteArray() );

    numberPrecision = _settings->value("calcspecs/decimalplaces", 6).toUInt();
    lambdaBegin = _settings->value("calcspecs/startwavelength", 0.01).toDouble();
    lambdaEnd = _settings->value("calcspecs/stopwavelength", 20.0).toDouble();
    nLambda = _settings->value("calcspecs/samplingpoints", 1000).toUInt();
    max_hkl = _settings->value("calcspecs/maxhkl", 8).toUInt();
    doubleSpinBox_sampleTemp->setValue( _settings->value("calcspecs/sampletemperature", 293.0).toDouble() );

    action_auto_resize->setChecked( _settings->value("diagram/axesautoscale", true).toBool() );
    showLegend->setChecked( _settings->value("diagram/showlegend", true).toBool() );

    for( int i=0; i<xAxisScaling->actions().size(); i++ )
    {
      if( xAxisScaling->actions().at(i)->text().compare( _settings->value("diagram/xaxisscale", "Linear").toString() ) == 0 )
      {
        xAxisScaling->actions().at(i)->trigger();
        break;
      }
    }

    for( int i=0; i<yAxisScaling->actions().size(); i++ )
    {
      if( yAxisScaling->actions().at(i)->text().compare( _settings->value("diagram/yaxisscale", "Linear").toString() ) == 0 )
      {
        yAxisScaling->actions().at(i)->trigger();
        break;
      }
    }

    for( int i=0; i<xAxisUnit->actions().size(); i++ )
    {
      if( xAxisUnit->actions().at(i)->text().compare( _settings->value("diagram/xaxisunit", "Neutron energy [eV]").toString() ) == 0 )
      {
        xAxisUnit->actions().at(i)->trigger();
        break;
      }
    }

  }
  else
  {
    _settings->clear();
  }
}


/*!
    \fn MainWindow::plotCrosssections()
 */
/**
 * This function plots a new diagram by setting the sample parameter before and calculating the cross sections.
 */
void MainWindow::plotCrosssections()
{
  statusBar()->showMessage(  "Calculating..." );
  setSampleProperties();
  toolButton_plot->setDisabled( true );
  groupBox_diagram->setDisabled( true );
  calc.moveToThread( &thread );
  calc.setCalculationRange( lambdaBegin, lambdaEnd, nLambda );
  calc.setSampleProperties( doubleSpinBox_packingfactor->value(), doubleSpinBox_samplethickness->value()/10.0, doubleSpinBox_sampleTemp->value() );

  thread.start();
}



/*!
    \fn MainWindow::setSampleProperties()
 */
/**
 * This function sets the sample parameters from the GUI and sends them to the BraggEdgeCalculator object.
 */
void MainWindow::setSampleProperties()
{
  calc.unitcell = nxs_newUnitCell();
  calc.unitcell.a = doubleSpinBox_lattice_a->value();
  calc.unitcell.b = doubleSpinBox_lattice_b->value();
  calc.unitcell.c = doubleSpinBox_lattice_c->value();
  calc.unitcell.alpha = doubleSpinBox_lattice_alpha->value();
  calc.unitcell.beta = doubleSpinBox_lattice_beta->value();
  calc.unitcell.gamma = doubleSpinBox_lattice_gamma->value();
#ifdef _MSC_VER
  int length = comboBox_spaceGroupNo->currentText().toLatin1().length() + 1;
  strcpy_s( calc.unitcell.spaceGroup, length, comboBox_spaceGroupNo->currentText().toLatin1().constData() );
#else
  strcpy( calc.unitcell.spaceGroup,comboBox_spaceGroupNo->currentText().toLatin1().constData() );

#endif
  calc.unitcell.debyeTemp = doubleSpinBox_debyetemp->value();
  calc.unitcell.mph_c2 = doubleSpinBox_mph_c2->value();


  calc.unitcell.maxHKL_index = max_hkl;
  nxs_initUnitCell( &(calc.unitcell) );
  calc.unitcell.temperature = doubleSpinBox_sampleTemp->value();

  pushButton_listPositions->setEnabled( true );

  // add atom infos
  NXS_AtomInfo ai;
  for( int i=0; i<tableWidget_atoms->rowCount(); i++ )
  {
#ifdef _MSC_VER
    length = tableWidget_atoms->item( i, 0 )->text().toLatin1().length() + 1;
    strcpy_s( ai.label, length, tableWidget_atoms->item( i, 0 )->text().toLatin1().constData() );
#else
    strcpy( ai.label, tableWidget_atoms->item( i, 0 )->text().toLatin1().constData() );
#endif
    ai.b_coherent = tableWidget_atoms->item( i, 1 )->data(Qt::EditRole).toDouble();
    ai.sigmaIncoherent = tableWidget_atoms->item( i, 2 )->data(Qt::EditRole).toDouble();
    ai.sigmaAbsorption = tableWidget_atoms->item( i, 3 )->data(Qt::EditRole).toDouble();
    ai.molarMass = tableWidget_atoms->item( i, 4 )->data(Qt::EditRole).toDouble();

    double tmp = tableWidget_atoms->item( i, 5 )->data(Qt::EditRole).toDouble();
    if( tmp > 1.0 )
    {
      ai.debyeTemp = tmp;
      ai.siteOccupation = 1.0;
    }
    else
    {
      ai.debyeTemp = -1.0;
      ai.siteOccupation = tmp;
    }
    ai.x[0] = tableWidget_atoms->item( i, 6 )->data(Qt::EditRole).toDouble();
    ai.y[0] = tableWidget_atoms->item( i, 7 )->data(Qt::EditRole).toDouble();
    ai.z[0] = tableWidget_atoms->item( i, 8 )->data(Qt::EditRole).toDouble();
    nxs_addAtomInfo( &(calc.unitcell), ai );
  }

  nxs_initHKL( &(calc.unitcell) );

  // add texture
  textureVector.clear();
  if( checkBox_enableTexture->checkState()==Qt::Checked )
  {
    for( int i=0; i<tableWidget_texture->rowCount(); i++ )
    {
      NXS_Texture tex;
      tex.a = tableWidget_texture->item( i, 0 )->data(Qt::EditRole).toInt();
      tex.b = tableWidget_texture->item( i, 1 )->data(Qt::EditRole).toInt();
      tex.c = tableWidget_texture->item( i, 2 )->data(Qt::EditRole).toInt();
      tex.r = tableWidget_texture->item( i, 3 )->data(Qt::EditRole).toDouble();
      tex.f = tableWidget_texture->item( i, 4 )->data(Qt::EditRole).toDouble();
      textureVector.push_back( tex );
    }
  }
  calc.setMarchDollaseTexture( &textureVector );
}



/*!
    \fn MainWindow::readSamplesFile()
 */
void MainWindow::readSamplesFile()
{
  disconnect( comboBox_entries, SIGNAL(currentIndexChanged(QString)), this, SLOT(refreshForm(QString)) );
  comboBox_entries->clear();
  connect( comboBox_entries, SIGNAL(currentIndexChanged(QString)), this, SLOT(refreshForm(QString)) );

  QFileInfo fi = QFileInfo( inputFileName );
  QFile file( inputFileName );
  if ( !fi.exists() || !fi.isReadable() || !file.open(QFile::ReadOnly | QFile::Text) )
  {
    QMessageBox::warning(this,  "Warning!",  "Cannot read file "+inputFileName+":\n"+file.errorString()+".");
  }
  else
  {
    QSettings settings( inputFileName, QSettings::IniFormat );
    QStringList examplesList = settings.childGroups();
    examplesList.sort();
    comboBox_entries->addItems( examplesList );
  }
}



/*!
    \fn MainWindow::writeSamplesFile()
 */
void MainWindow::writeSamplesFile()
{
  QString entry = comboBox_entries->currentText();
  QMessageBox::StandardButton ret;
  ret = QMessageBox::warning(this,  "nxsPlotter",
                      "Are you sure to save \""+entry+"\"?\n(Existing values will be overwritten!)",
                      QMessageBox::Yes | QMessageBox::No);
  if ( ret==QMessageBox::Yes )
  {
    QSettings settings( inputFileName, QSettings::IniFormat );
    int numPrec = int(numberPrecision);
    settings.remove( entry );
    settings.setValue( entry+"/space_group", comboBox_spaceGroupNo->currentText() );
    settings.setValue( entry+"/lattice_a", QString::number(doubleSpinBox_lattice_a->value(),'g',numPrec) );
    settings.setValue( entry+"/lattice_b", QString::number(doubleSpinBox_lattice_b->value(),'g',numPrec) );
    settings.setValue( entry+"/lattice_c", QString::number(doubleSpinBox_lattice_c->value(),'g',numPrec) );
    settings.setValue( entry+"/lattice_alpha", QString::number(doubleSpinBox_lattice_alpha->value(),'g',numPrec) );
    settings.setValue( entry+"/lattice_beta", QString::number(doubleSpinBox_lattice_beta->value(),'g',numPrec) );
    settings.setValue( entry+"/lattice_gamma", QString::number(doubleSpinBox_lattice_gamma->value(),'g',numPrec) );
    settings.setValue( entry+"/debye_temp", QString::number(doubleSpinBox_debyetemp->value(),'g',numPrec) );
    settings.setValue( entry+"/mph_c2", QString::number(doubleSpinBox_mph_c2->value(),'g',numPrec) );

    for( int i=0; i<tableWidget_atoms->rowCount(); i++ )
    {
      QString atomString;
      atomString = tableWidget_atoms->item( i, 0 )->text();
      atomString += " " + QString::number(tableWidget_atoms->item( i, 1 )->data(Qt::EditRole).toDouble(),'g',numPrec);
      atomString += " " + QString::number(tableWidget_atoms->item( i, 2 )->data(Qt::EditRole).toDouble(),'g',numPrec);
      atomString += " " + QString::number(tableWidget_atoms->item( i, 3 )->data(Qt::EditRole).toDouble(),'g',numPrec);
      atomString += " " + QString::number(tableWidget_atoms->item( i, 4 )->data(Qt::EditRole).toDouble(),'g',numPrec);
      atomString += " " + QString::number(tableWidget_atoms->item( i, 5 )->data(Qt::EditRole).toDouble(),'g',numPrec);
      atomString += " " + QString::number(tableWidget_atoms->item( i, 6 )->data(Qt::EditRole).toDouble(),'g',numPrec);
      atomString += " " + QString::number(tableWidget_atoms->item( i, 7 )->data(Qt::EditRole).toDouble(),'g',numPrec);
      atomString += " " + QString::number(tableWidget_atoms->item( i, 8 )->data(Qt::EditRole).toDouble(),'g',numPrec);
      QString num;
      settings.setValue( entry+"/atom"+num.setNum(i), atomString );
    }

  disconnect( comboBox_entries, SIGNAL(currentIndexChanged(QString)), this, SLOT(refreshForm(QString)) );
  comboBox_entries->clear();
  QStringList examplesList = settings.childGroups();
  examplesList.sort();
  comboBox_entries->addItems( examplesList );
  comboBox_entries->setCurrentIndex( comboBox_entries->findText(entry) );
  connect( comboBox_entries, SIGNAL(currentIndexChanged(QString)), this, SLOT(refreshForm(QString)) );
 }
}



/*!
    \fn MainWindow::deleteSample()
 */
void MainWindow::deleteSample()
{
  QString entry = comboBox_entries->currentText();
  QMessageBox::StandardButton ret;
  ret = QMessageBox::warning(this,  "nxsPlotter",
                      "Are you sure to delete \""+entry+"\"?\n",
                      QMessageBox::Yes | QMessageBox::No);
  if ( ret==QMessageBox::Yes )
  {
    comboBox_entries->removeItem( comboBox_entries->currentIndex() );
    QSettings settings( inputFileName, QSettings::IniFormat );
    settings.remove( entry );
  }
}



/*!
    \fn MainWindow::refreshForm( QString )
 */
void MainWindow::refreshForm( QString entry )
{
  QSettings settings( inputFileName, QSettings::IniFormat );
  doubleSpinBox_lattice_a->setValue( settings.value(entry+"/lattice_a", 0.0).toDouble() );
  doubleSpinBox_lattice_b->setValue( settings.value(entry+"/lattice_b", 0.0).toDouble() );
  doubleSpinBox_lattice_c->setValue( settings.value(entry+"/lattice_c", 0.0).toDouble() );
  doubleSpinBox_lattice_alpha->setValue( settings.value(entry+"/lattice_alpha", 0.0).toDouble() );
  doubleSpinBox_lattice_beta->setValue( settings.value(entry+"/lattice_beta", 0.0).toDouble() );
  doubleSpinBox_lattice_gamma->setValue( settings.value(entry+"/lattice_gamma", 0.0).toDouble() );
  doubleSpinBox_debyetemp->setValue( settings.value(entry+"/debye_temp", 0.0).toDouble() );
  doubleSpinBox_mph_c2->setValue( settings.value(entry+"/mph_c2", 0.0).toDouble() );

  int index = comboBox_spaceGroupNo->findText( settings.value(entry+"/space_group").toString() );
  comboBox_spaceGroupNo->setCurrentIndex( 0 );
  comboBox_spaceGroupNo->setCurrentIndex( index );

  settings.beginGroup( entry );

  while( tableWidget_atoms->rowCount() )
    tableWidget_atoms->removeRow( tableWidget_atoms->rowCount()-1 );

  QStringList atomInfos = settings.childKeys().filter( QRegExp("^atom\\d+$") );
  for( int i=0; i<atomInfos.count(); i++ )
  {
    tableWidget_atoms->insertRow( i );
    tableWidget_atoms->setRowHeight( i, 20 );

    QString text = settings.value( atomInfos.at(i) ).toString();
    QStringList values = text.split( " " );

    QTableWidgetItem *item = new QTableWidgetItem( values.at(0) );
    tableWidget_atoms->setItem( i, 0, item );

//    for( int j; j<=8; j++ )
    item = new QTableWidgetItem();
    item->setData( Qt::EditRole, values.at(1) );
    tableWidget_atoms->setItem( i, 1, item );
    item = new QTableWidgetItem();
    item->setData( Qt::EditRole, values.at(2) );
    tableWidget_atoms->setItem( i, 2, item );
    item = new QTableWidgetItem();
    item->setData( Qt::EditRole, values.at(3) );
    tableWidget_atoms->setItem( i, 3, item );
    item = new QTableWidgetItem();
    item->setData( Qt::EditRole, values.at(4) );
    tableWidget_atoms->setItem( i, 4, item );
    item = new QTableWidgetItem();
    item->setData( Qt::EditRole, values.at(5) );
    tableWidget_atoms->setItem( i, 5, item );
    item = new QTableWidgetItem();
    item->setData( Qt::EditRole, values.at(6) );
    tableWidget_atoms->setItem( i, 6, item );
    item = new QTableWidgetItem();
    item->setData( Qt::EditRole, values.at(7) );
    tableWidget_atoms->setItem( i, 7, item );
    item = new QTableWidgetItem();
    item->setData( Qt::EditRole, values.at(8) );
    tableWidget_atoms->setItem( i, 8, item );
  }

  tableWidget_atoms->resizeColumnsToContents();
  if ( tableWidget_atoms->rowCount() > 0 )
    toolButton_plot->setEnabled( true );
}



/*!
    \fn MainWindow::disableLatticeBox( QString )
 */
void MainWindow::disableLatticeBox( QString spaceGroup )
{
  const T_TabSgName *tsgn;
  const char *sgNumber = qstrdup( spaceGroup.toLatin1() );
  T_SgInfo SgInfo;
  SgInfo.MaxList = 192;
  SgInfo.ListSeitzMx = reinterpret_cast<T_RTMx*>( malloc(unsigned(SgInfo.MaxList) * sizeof(*SgInfo.ListSeitzMx)) );
  SgInfo.ListRotMxInfo = nullptr;
  tsgn = FindTabSgNameEntry( sgNumber, 'A' );
  InitSgInfo( &SgInfo );
  SgInfo.TabSgName = tsgn;
  if ( tsgn )
    SgInfo.GenOption = 1;
  ParseHallSymbol( tsgn->HallSymbol, &SgInfo );
  CompleteSgInfo( &SgInfo );

  switch( SgInfo.XtalSystem )
  {
    /* XS_Cubic */
    case 7:   doubleSpinBox_lattice_a->setEnabled( true );
              doubleSpinBox_lattice_b->setEnabled( false );
              doubleSpinBox_lattice_c->setEnabled( false );
              doubleSpinBox_lattice_alpha->setEnabled( false );
              doubleSpinBox_lattice_alpha->setValue( 90.0 );
              doubleSpinBox_lattice_beta->setEnabled( false );
              doubleSpinBox_lattice_beta->setValue( 90.0 );
              doubleSpinBox_lattice_gamma->setEnabled( false );
              doubleSpinBox_lattice_gamma->setValue( 90.0 );
              break;
    /* XS_Hexagonal */
    case 6:   doubleSpinBox_lattice_a->setEnabled( true );
              doubleSpinBox_lattice_b->setEnabled( false );
              doubleSpinBox_lattice_c->setEnabled( true );
              doubleSpinBox_lattice_alpha->setEnabled( false );
              doubleSpinBox_lattice_alpha->setValue( 90.0 );
              doubleSpinBox_lattice_beta->setEnabled( false );
              doubleSpinBox_lattice_beta->setValue( 90.0 );
              doubleSpinBox_lattice_gamma->setEnabled( false );
              doubleSpinBox_lattice_gamma->setValue( 120.0 );
              break;
    /* XS_Trigonal */
    case 5:   doubleSpinBox_lattice_a->setEnabled( true );
              doubleSpinBox_lattice_b->setEnabled( false );
              doubleSpinBox_lattice_c->setEnabled( true );
              doubleSpinBox_lattice_alpha->setEnabled( false );
              doubleSpinBox_lattice_alpha->setValue( 90.0 );
              doubleSpinBox_lattice_beta->setEnabled( false );
              doubleSpinBox_lattice_beta->setValue( 90.0 );
              doubleSpinBox_lattice_gamma->setEnabled( false );
              doubleSpinBox_lattice_gamma->setValue( 120.0 );
              break;
    /* XS_Tetragonal */
    case 4:   doubleSpinBox_lattice_a->setEnabled( true );
              doubleSpinBox_lattice_b->setEnabled( false );
              doubleSpinBox_lattice_c->setEnabled( true );
              doubleSpinBox_lattice_alpha->setEnabled( false );
              doubleSpinBox_lattice_alpha->setValue( 90.0 );
              doubleSpinBox_lattice_beta->setEnabled( false );
              doubleSpinBox_lattice_beta->setValue( 90.0 );
              doubleSpinBox_lattice_gamma->setEnabled( false );
              doubleSpinBox_lattice_gamma->setValue( 90.0 );
              break;
    /* XS_Orthorhombic */
    case 3:   doubleSpinBox_lattice_a->setEnabled( true );
              doubleSpinBox_lattice_b->setEnabled( true );
              doubleSpinBox_lattice_c->setEnabled( true );
              doubleSpinBox_lattice_alpha->setEnabled( false );
              doubleSpinBox_lattice_alpha->setValue( 90.0 );
              doubleSpinBox_lattice_beta->setEnabled( false );
              doubleSpinBox_lattice_beta->setValue( 90.0 );
              doubleSpinBox_lattice_gamma->setEnabled( false );
              doubleSpinBox_lattice_gamma->setValue( 90.0 );
              break;
    /* XS_Monoclinic */
    case 2:   doubleSpinBox_lattice_a->setEnabled( true );
              doubleSpinBox_lattice_b->setEnabled( true );
              doubleSpinBox_lattice_c->setEnabled( true );
              doubleSpinBox_lattice_alpha->setEnabled( false );
              doubleSpinBox_lattice_alpha->setValue( 90.0 );
              doubleSpinBox_lattice_beta->setEnabled( true );
              doubleSpinBox_lattice_gamma->setEnabled( false );
              doubleSpinBox_lattice_gamma->setValue( 90.0 );
              break;
    /* XS_Triclinic */
    case 1:   doubleSpinBox_lattice_a->setEnabled( true );
              doubleSpinBox_lattice_b->setEnabled( true );
              doubleSpinBox_lattice_c->setEnabled( true );
              doubleSpinBox_lattice_alpha->setEnabled( true );
              doubleSpinBox_lattice_beta->setEnabled( true );
              doubleSpinBox_lattice_gamma->setEnabled( true );
              break;
    /* XS_Unknown */
    default:  doubleSpinBox_lattice_a->setEnabled( true );
              doubleSpinBox_lattice_b->setEnabled( true );
              doubleSpinBox_lattice_c->setEnabled( true );
              doubleSpinBox_lattice_alpha->setEnabled( true );
              doubleSpinBox_lattice_beta->setEnabled( true );
              doubleSpinBox_lattice_gamma->setEnabled( true );
              break;
  }
}

void MainWindow::toggleLegend(bool show)
{
  widget_plot_attenuation->legend->setVisible( show );
  widget_plot_attenuation->replot();
  widget_plot_crosssections->legend->setVisible( show );
  widget_plot_crosssections->replot();

}





/*!
    \fn MainWindow::savePlotValues()
 */
/**
 *  This function saves the plot values from the diagram to a tabulated text file. A file dialog will be called.
 */
void MainWindow::savePlotValues()
{
  QString fileName = QFileDialog::getSaveFileName( this,  "Save Plot" );
  if ( fileName.isEmpty() )
    return;

  QFile file( fileName );
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    return;

  QTextStream out( &file );
  QString text, str1, str2;
  str1.setNum( numberPrecision );
  str2.setNum( numberPrecision+8 );

  if( stackedWidget_plots->currentIndex() == 0 )
  {
    str1 = "%."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f";

    out << "# lambda[A]  coh_el[barns]  coh_inel[barns]  inc_el[barns]  inc_inel[barns]  abs[barns]  total[barns]" << endl;
    out.setRealNumberPrecision( int(numberPrecision) );
    out.setRealNumberNotation( QTextStream::FixedNotation );

    for( unsigned int i=0; i<calc.nLambda; i++ )
      out << str2.sprintf( qstrdup( str1.toLatin1() ), calc.vdLambda[i], calc.vdXsectCohEl[i], calc.vdXsectCohInel[i], calc.vdXsectIncEl[i], calc.vdXsectIncInel[i], calc.vdXsectAbs[i], calc.vdXsectTotal[i] ) << endl;
  }
  else if( stackedWidget_plots->currentIndex() == 1 )
  {
    str1 = "%."+str1+"f "+"%"+str2+"."+str1+"f";

    out << "# lambda[A]  att.coeff.[cm-1]" << endl;
    out.setRealNumberPrecision( int(numberPrecision) );
    out.setRealNumberNotation( QTextStream::FixedNotation );

    for( unsigned int i=0; i<calc.nLambda; i++ )
      out << str2.sprintf( qstrdup( str1.toLatin1() ), calc.vdLambda[i], calc.vdAttenCoeff[i] ) << endl;
  }

  file.close();
}



/*!
    \fn MainWindow::listPlotValues()
 */
/**
 *  This function calls a dialog where all plot values will be listed in a tabulated format.
 */
void MainWindow::listPlotValues()
{
  ListValuesDialog dialog( this );
  dialog.setWindowTitle( "Plot values - " + comboBox_entries->currentText() );

  QString text, str1, str2;
  str1.setNum( numberPrecision );
  str2.setNum( numberPrecision+8 );
  QTextStream out(&text);

  if( stackedWidget_plots->currentIndex() == 0 )
  {
    str1 = "%."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f "+"%"+str2+"."+str1+"f";

    out << "# lambda[A]  coh_el[barns]  coh_inel[barns]  inc_el[barns]  inc_inel[barns]  abs[barns]  total[barns]" << endl;
    out.setRealNumberPrecision( int(numberPrecision) );
    out.setRealNumberNotation( QTextStream::FixedNotation );

    for( unsigned int i=0; i<calc.nLambda; i++ )
      out << str2.sprintf( qstrdup( str1.toLatin1() ), calc.vdLambda[i], calc.vdXsectCohEl[i], calc.vdXsectCohInel[i], calc.vdXsectIncEl[i], calc.vdXsectIncInel[i], calc.vdXsectAbs[i], calc.vdXsectTotal[i] ) << endl;
  }
  else if( stackedWidget_plots->currentIndex() == 1 )
  {
    str1 = "%."+str1+"f "+"%"+str2+"."+str1+"f";

    out << "# lambda[A]  att.coeff.[cm-1]" << endl;
    out.setRealNumberPrecision( int(numberPrecision) );
    out.setRealNumberNotation( QTextStream::FixedNotation );

    for( unsigned int i=0; i<calc.nLambda; i++ )
      out << str2.sprintf( qstrdup( str1.toLatin1() ), calc.vdLambda[i], calc.vdAttenCoeff[i] ) << endl;
  }

  dialog.setValuesText( text );
  dialog.exec();
}



/*!
    \fn MainWindow::listHKLValues()
 */
/**
 *  This function calls a dialog where all hkl reflection values will be listed in a tabulated format.
 */
void MainWindow::listHKLValues()
{
  setSampleProperties();

  ListValuesDialog dialog( this );
  dialog.setWindowTitle( "HKL list - "+comboBox_entries->currentText() );
  QString text, str1, str2;
  str1.setNum( numberPrecision );
  str2.setNum( numberPrecision+8 );
  str1 = "%2i %2i %2i  %8i %"+str2+"."+str1+"f %"+str2+"."+str1+"f";

  QTextStream out(&text);
  out << "# hkl     multiplicity     d_hkl     |F|^2" << endl;
  out.setRealNumberPrecision( int(numberPrecision) );
  out.setRealNumberNotation( QTextStream::FixedNotation );
  for( unsigned int i=0; i<calc.unitcell.nHKL; i++ )
    out << str2.sprintf( qstrdup( str1.toLatin1() ), calc.unitcell.hklList[i].h, calc.unitcell.hklList[i].k, calc.unitcell.hklList[i].l, calc.unitcell.hklList[i].multiplicity, calc.unitcell.hklList[i].dhkl, calc.unitcell.hklList[i].FSquare) << endl;


  dialog.setValuesText( text );
  dialog.exec();
}







/*!
    \fn MainWindow::changeStyle( QAction )
 */
/**
 * This function changes the Qt style of the complete GUI.
 * @param action_style User-interaction object with style information inside
 */
void MainWindow::changeStyle( QAction *action_style )
{
  QApplication::setStyle( QStyleFactory::create( action_style->text() ) );
}



/**
 * This function will call a dialog asking if you really want to quit the program.
 * @param event a QCloseEvent called by the user
 */
void MainWindow::closeEvent(QCloseEvent *event)
{
  QMessageBox::StandardButton ret;
  ret = QMessageBox::warning(this,  "nxsPlotter",
                       "Do you really want to quit?\n",
                      QMessageBox::Yes | QMessageBox::No);

  if ( ret == QMessageBox::Yes )
  {

    _settings->setValue("nxsPlotter/version", QString::number(this->_programversion,'f',1) );
    _settings->setValue( "window/state", this->saveState() );
    _settings->setValue( "window/splitter", this->splitter->saveState() );

    if( this->isMaximized() )
      _settings->setValue( "window/fullscreen", this->isMaximized() );
    else
    {
      _settings->setValue( "window/size", this->size() );
      _settings->setValue( "window/position", this->pos() );
      _settings->setValue( "window/fullscreen", this->isMaximized() );
    }

    _settings->setValue( "calcspecs/decimalplaces", numberPrecision );
    _settings->setValue( "calcspecs/startwavelength", lambdaBegin );
    _settings->setValue( "calcspecs/stopwavelength", lambdaEnd );
    _settings->setValue( "calcspecs/samplingpoints", nLambda );
    _settings->setValue( "calcspecs/maxhkl", max_hkl );
    _settings->setValue( "calcspecs/sampletemperature", doubleSpinBox_sampleTemp->value() );

    _settings->setValue( "diagram/axesautoscale", action_auto_resize->isChecked() );
    _settings->setValue( "diagram/showlegend", showLegend->isChecked() );
    _settings->setValue( "diagram/xaxisscale", xAxisScaling->checkedAction()->text() );
    _settings->setValue( "diagram/xaxisunit", xAxisUnit->checkedAction()->text() );
    _settings->setValue( "diagram/yaxisscale", yAxisScaling->checkedAction()->text() );

    event->accept();
  }
  else
    event->ignore();
}




/*!
    \fn MainWindow::on_action_number_precision_triggered()
 */
/**
 * This function calls a dialog to set the number precision (decimal places).
 */
void MainWindow::on_action_number_precision_triggered()
{
  NumberPrecisionDialog dlg( this );
  dlg.setNumberPrecision( int(numberPrecision) );
  dlg.setStartWavelength( lambdaBegin );
  dlg.setStopWavelength( lambdaEnd );
  dlg.setSamplingPoints( nLambda);
  dlg.setMaxHKL( max_hkl );
  if ( dlg.exec() )
  {
    changeNumberPrecision();
    numberPrecision = dlg.getNumberPrecision();

    lambdaBegin = dlg.getStartWavelength();
    lambdaEnd = dlg.getStopWavelength();
    nLambda = dlg.getSamplingPoints();
    max_hkl = dlg.getMaxHKL();
  }
}

/*!
    \fn MainWindow::changeNumberPrecision()
 */
/**
 * This function sets the number precision (decimal places) for all values used in this program.
 */
void MainWindow::changeNumberPrecision()
{
    int numPrec = int(numberPrecision);
    doubleSpinBox_lattice_a->setDecimals( numPrec );
    doubleSpinBox_lattice_b->setDecimals( numPrec );
    doubleSpinBox_lattice_c->setDecimals( numPrec );
    doubleSpinBox_lattice_alpha->setDecimals( numPrec );
    doubleSpinBox_lattice_beta->setDecimals( numPrec );
    doubleSpinBox_lattice_gamma->setDecimals( numPrec );

    tableWidget_atoms->setItemDelegateForColumn( 1, new DoubleSpinBoxDelegate( numberPrecision ) );
    tableWidget_atoms->setItemDelegateForColumn( 2, new DoubleSpinBoxDelegate( numberPrecision ) );
    tableWidget_atoms->setItemDelegateForColumn( 3, new DoubleSpinBoxDelegate( numberPrecision ) );
    tableWidget_atoms->setItemDelegateForColumn( 4, new DoubleSpinBoxDelegate( numberPrecision ) );
    tableWidget_atoms->setItemDelegateForColumn( 5, new DoubleSpinBoxDelegate( numberPrecision ) );
    tableWidget_atoms->setItemDelegateForColumn( 6, new DoubleSpinBoxDelegate( numberPrecision ) );
    tableWidget_atoms->setItemDelegateForColumn( 7, new DoubleSpinBoxDelegate( numberPrecision ) );
    tableWidget_atoms->setItemDelegateForColumn( 8, new DoubleSpinBoxDelegate( numberPrecision ) );
}


/*!
    \fn MainWindow::changePlotColors()
 */
/**
 * This function calls a dialog to change the plot colors of the diagram.
 */
void MainWindow::changePlotColors()
{
  PlotColorsDialog dlg( this );
  dlg.setColors( (widget_plot_crosssections->graph(0)->pen()).color(), (widget_plot_crosssections->graph(1)->pen()).color(), (widget_plot_crosssections->graph(2)->pen()).color(), (widget_plot_crosssections->graph(3)->pen()).color(), (widget_plot_crosssections->graph(4)->pen()).color(), (widget_plot_crosssections->graph(5)->pen()).color() );

  if ( dlg.exec() )
  {
    widget_plot_crosssections->graph(0)->setPen( QPen(dlg.color_coh_el()) );
    widget_plot_crosssections->graph(1)->setPen( QPen(dlg.color_coh_inel()) );
    widget_plot_crosssections->graph(2)->setPen( QPen(dlg.color_inc_el()) );
    widget_plot_crosssections->graph(3)->setPen( QPen(dlg.color_inc_inel()) );
    widget_plot_crosssections->graph(4)->setPen( QPen(dlg.color_absorption()) );
    widget_plot_crosssections->graph(5)->setPen( QPen(dlg.color_total()) );
    widget_plot_crosssections->replot();
  }
}


/*!
    \fn MainWindow::changeInputFile()
 */
/**
 * This function calls a dialog to change the input example file.
 */
void MainWindow::changeInputFile()
{
  InputFileDialog dlg( this );
  dlg.setFileName( inputFileName );

  if ( dlg.exec() )
  {
    inputFileName = dlg.getFileName();
    readSamplesFile();
  }
}


/*!
    \fn MainWindow::initSpaceGroups()
 */
/**
 * This function initialises all space groups and puts them into a QComboBox.
 * \see SgInfo documentation also.
 */
void MainWindow::initSpaceGroups()
{
  comboBox_spaceGroupNo->clear();

  // find space group denotation from the SgInfo library
  const T_TabSgName *tsgn;
  for( tsgn=TabSgName; tsgn->HallSymbol; tsgn++ )
  {
    QString sgNumber, sgHermann;
    comboBox_spaceGroupHermann->addItem( tsgn->SgLabels );
    comboBox_spaceGroupHall->addItem( tsgn->HallSymbol );

    sgNumber.setNum( tsgn->SgNumber );
    QString extension( tsgn->Extension );
    if( !extension.isEmpty() )
      comboBox_spaceGroupNo->addItem( sgNumber+":"+extension );
    else
      comboBox_spaceGroupNo->addItem( sgNumber );
  }
  comboBox_spaceGroupNo->setMaxVisibleItems( 10 );

  // connect the comboboxes
  connect( comboBox_spaceGroupNo, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupNoIndex(int)) );
  connect( comboBox_spaceGroupHall, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHallIndex(int)) );
  connect( comboBox_spaceGroupHermann, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHermannIndex(int)) );
}



void MainWindow::about()
{
  //QMessageBox::about(this,  "nxsPlotter v0.90"),  "created by Mirko Boin, ") + QDate::currentDate().toString("MMMM yyyy") + (", Helmholtz Centre Berlin for Materials and Energy.") );
  AboutDialog dialog( this );
  //dialog.setWindowTitle( "Atom position list - "+comboBox_entries->currentText() );
  dialog.exec();
}




/*!
    \fn MainWindow::on_pushButton_plus_clicked()
 */
void MainWindow::on_pushButton_plus_clicked()
{
  tableWidget_atoms->insertRow( tableWidget_atoms->rowCount() );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 0, new QTableWidgetItem("") );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 1, new QTableWidgetItem("0") );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 2, new QTableWidgetItem("0") );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 3, new QTableWidgetItem("0") );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 4, new QTableWidgetItem("0") );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 5, new QTableWidgetItem("0") );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 6, new QTableWidgetItem("0") );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 7, new QTableWidgetItem("0") );
  tableWidget_atoms->setItem( tableWidget_atoms->rowCount()-1 , 8, new QTableWidgetItem("0") );
  tableWidget_atoms->setRowHeight( tableWidget_atoms->rowCount()-1, 20 );

  if( tableWidget_atoms->rowCount() > 0 )
    toolButton_plot->setEnabled( true );
}




/*!
    \fn MainWindow::on_pushButton_minus_clicked()
 */
void MainWindow::on_pushButton_minus_clicked()
{
  if ( tableWidget_atoms->rowCount() > 0 )
    tableWidget_atoms->removeRow( tableWidget_atoms->currentRow() );

  if( tableWidget_atoms->rowCount() == 0 )
    toolButton_plot->setDisabled( true );
}



/*!
    \fn MainWindow::on_pushButton_listPositions_clicked()
 */
void MainWindow::on_pushButton_listPositions_clicked()
{
  ListValuesDialog dialog( this );
  dialog.setWindowTitle( "Atom position list - "+comboBox_entries->currentText() );
  QString text, str1, str2;
  str1.setNum( numberPrecision );
  str2.setNum( numberPrecision+8 );
  str1 = "%s %"+str2+"."+str1+"f %"+str2+"."+str1+"f %"+str2+"."+str1+"f";

  QTextStream out(&text);
  out << "# Atom     x           y          z" << endl;
  out.setRealNumberPrecision( int(numberPrecision) );
  out.setRealNumberNotation( QTextStream::FixedNotation );
  for ( unsigned int i=0; i<calc.unitcell.nAtomInfo; i++ )
  {
    for ( unsigned int j=0; j<calc.unitcell.atomInfoList[i].nAtoms; j++ )
    {
      out << str2.sprintf( qstrdup( str1.toLatin1() ), calc.unitcell.atomInfoList[i].label, calc.unitcell.atomInfoList[i].x[j], calc.unitcell.atomInfoList[i].y[j], calc.unitcell.atomInfoList[i].z[j] ) << endl;
    }
  }

  dialog.setValuesText( text );
  dialog.exec();
}



/*!
    \fn MainWindow::changeSpaceGroupNoIndex( int index )
 */
void MainWindow::changeSpaceGroupNoIndex( int index )
{
  disconnect( comboBox_spaceGroupNo, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHallIndex(int)) );
  disconnect( comboBox_spaceGroupNo, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHermannIndex(int)) );
  comboBox_spaceGroupHall->setCurrentIndex( index );
  comboBox_spaceGroupHermann->setCurrentIndex( index );
  connect( comboBox_spaceGroupNo, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHallIndex(int)) );
  connect( comboBox_spaceGroupNo, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHermannIndex(int)) );
}



/*!
    \fn MainWindow::changeSpaceGroupHermannIndex( int index )
 */
void MainWindow::changeSpaceGroupHermannIndex( int index )
{
  disconnect( comboBox_spaceGroupHermann, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHallIndex(int)) );
  disconnect( comboBox_spaceGroupHermann, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupNoIndex(int)) );
  comboBox_spaceGroupHall->setCurrentIndex( index );
  comboBox_spaceGroupNo->setCurrentIndex( index );
  connect( comboBox_spaceGroupHermann, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHallIndex(int)) );
  connect( comboBox_spaceGroupHermann, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupNoIndex(int)) );
}



/*!
    \fn MainWindow::changeSpaceGroupHallIndex( int index )
 */
void MainWindow::changeSpaceGroupHallIndex( int index )
{
  disconnect( comboBox_spaceGroupHall, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupNoIndex(int)) );
  disconnect( comboBox_spaceGroupHall, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHermannIndex(int)) );
  comboBox_spaceGroupNo->setCurrentIndex( index );
  comboBox_spaceGroupHermann->setCurrentIndex( index );
  connect( comboBox_spaceGroupHall, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupNoIndex(int)) );
  connect( comboBox_spaceGroupHall, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSpaceGroupHermannIndex(int)) );
}





 /*!
    \fn MainWindow::on_tableWidget_selection_currentItemChanged( QTableWidgetItem * current, QTableWidgetItem * previous )
 */
void MainWindow::on_tableWidget_selection_currentItemChanged( QTableWidgetItem * current, QTableWidgetItem * previous )
{
  if (!current)
    current = previous;
  tableWidget_selection->setCurrentItem( current );
  stackedWidget->setCurrentIndex( tableWidget_selection->currentRow() );
}

/*!
    \fn MainWindow::on_action_showAttenuationCoeff_triggered( bool checkState )
*/
void MainWindow::on_action_showAttenuationCoeff_triggered( bool checkState )
{
  Q_UNUSED(checkState);
  stackedWidget_plots->setCurrentIndex( 1 );
}



/*!
    \fn MainWindow::on_action_showCrossSections_triggered( bool checkState )
*/
void MainWindow::on_action_showCrossSections_triggered( bool checkState )
{
  Q_UNUSED(checkState);
  stackedWidget_plots->setCurrentIndex( 0 );
}



/*!
    \fn MainWindow::on_action_showTransmission_triggered( bool checkState )
*/
void MainWindow::on_action_showTransmission_triggered( bool checkState )
{
  Q_UNUSED(checkState);
  stackedWidget_plots->setCurrentIndex( 2 );
}


/*!
    \fn MainWindow::on_pushButton_plusTexture_clicked()
 */
void MainWindow::on_pushButton_plusTexture_clicked()
{
  tableWidget_texture->insertRow( tableWidget_texture->rowCount() );
  tableWidget_texture->setItem( tableWidget_texture->rowCount()-1 , 0, new QTableWidgetItem("0") );
  tableWidget_texture->setItem( tableWidget_texture->rowCount()-1 , 1, new QTableWidgetItem("0") );
  tableWidget_texture->setItem( tableWidget_texture->rowCount()-1 , 2, new QTableWidgetItem("0") );
  tableWidget_texture->setItem( tableWidget_texture->rowCount()-1 , 3, new QTableWidgetItem("0") );
  tableWidget_texture->setItem( tableWidget_texture->rowCount()-1 , 4, new QTableWidgetItem("0") );
  tableWidget_texture->setRowHeight( tableWidget_texture->rowCount()-1, 20 );
}




/*!
    \fn MainWindow::on_pushButton_minusTexture_clicked()
 */
void MainWindow::on_pushButton_minusTexture_clicked()
{
  if( tableWidget_texture->rowCount() > 0 )
    tableWidget_texture->removeRow( tableWidget_texture->currentRow() );
}



/*!
    \fn MainWindow::on_checkBox_enableTexture_stateChanged( int state )
 */
void MainWindow::on_checkBox_enableTexture_stateChanged( int state )
{
  pushButton_plusTexture->setEnabled( bool(state) );
  pushButton_minusTexture->setEnabled( bool(state) );
  tableWidget_texture->setEnabled( bool(state) );
}


void MainWindow::mousePress()
{
  // if an axis is selected, only allow the direction of that axis to be dragged
  // if no axis is selected, both directions may be dragged
  if( widget_plot_crosssections->xAxis->selectedParts().testFlag(QCPAxis::spAxis) )
    widget_plot_crosssections->axisRect()->setRangeDrag( widget_plot_crosssections->xAxis->orientation() );
  else if( widget_plot_crosssections->yAxis->selectedParts().testFlag(QCPAxis::spAxis) )
    widget_plot_crosssections->axisRect()->setRangeDrag( widget_plot_crosssections->yAxis->orientation() );
  else
    widget_plot_crosssections->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);

  if( widget_plot_attenuation->xAxis->selectedParts().testFlag(QCPAxis::spAxis) )
    widget_plot_attenuation->axisRect()->setRangeDrag( widget_plot_attenuation->xAxis->orientation() );
  else if( widget_plot_attenuation->yAxis->selectedParts().testFlag(QCPAxis::spAxis) )
    widget_plot_attenuation->axisRect()->setRangeDrag( widget_plot_attenuation->yAxis->orientation() );
  else
    widget_plot_attenuation->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);

  if( widget_plot_transmission->xAxis->selectedParts().testFlag(QCPAxis::spAxis) )
    widget_plot_transmission->axisRect()->setRangeDrag( widget_plot_transmission->xAxis->orientation() );
  else if( widget_plot_transmission->yAxis->selectedParts().testFlag(QCPAxis::spAxis) )
    widget_plot_transmission->axisRect()->setRangeDrag( widget_plot_transmission->yAxis->orientation() );
  else
    widget_plot_transmission->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
}


void MainWindow::mouseWheel()
{
  // if an axis is selected, only allow the direction of that axis to be zoomed
  // if no axis is selected, both directions may be zoomed
  if( stackedWidget_plots->currentIndex() == 0 )
  {
    if( widget_plot_crosssections->xAxis->selectedParts().testFlag(QCPAxis::spAxis) )
      widget_plot_crosssections->axisRect()->setRangeZoom( widget_plot_crosssections->xAxis->orientation() );
    else if( widget_plot_crosssections->yAxis->selectedParts().testFlag(QCPAxis::spAxis) )
      widget_plot_crosssections->axisRect()->setRangeZoom( widget_plot_crosssections->yAxis->orientation() );
    else
      widget_plot_crosssections->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
  }
  else if( stackedWidget_plots->currentIndex() == 1 )
  {
    if( widget_plot_attenuation->xAxis->selectedParts().testFlag(QCPAxis::spAxis) )
      widget_plot_attenuation->axisRect()->setRangeZoom( widget_plot_attenuation->xAxis->orientation() );
    else if( widget_plot_attenuation->yAxis->selectedParts().testFlag(QCPAxis::spAxis) )
      widget_plot_attenuation->axisRect()->setRangeZoom( widget_plot_attenuation->yAxis->orientation() );
    else
      widget_plot_attenuation->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
  }
  else if( stackedWidget_plots->currentIndex() == 2 )
  {
    if( widget_plot_transmission->xAxis->selectedParts().testFlag(QCPAxis::spAxis) )
      widget_plot_transmission->axisRect()->setRangeZoom( widget_plot_transmission->xAxis->orientation() );
    else if( widget_plot_transmission->yAxis->selectedParts().testFlag(QCPAxis::spAxis) )
      widget_plot_transmission->axisRect()->setRangeZoom( widget_plot_transmission->yAxis->orientation() );
    else
      widget_plot_transmission->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
  }
}


void MainWindow::contextMenuRequest(QPoint pos)
{
  plotMenu->exec( stackedWidget_plots->currentWidget()->mapToGlobal(pos) );
}

void MainWindow::onThreadFinished()
{
  // give some results
  label_mass->setNum( calc.unitcell.mass );
  label_numAtoms->setNum( int(calc.unitcell.nAtoms) );
  label_density->setNum( calc.unitcell.density );
  label_unitCellVolume->setNum( calc.unitcell.volume );

  if( doubleSpinBox_mph_c2->value() < 1E-6 )
    doubleSpinBox_mph_c2->setValue( calc.unitcell.mph_c2 );

  if( doubleSpinBox_debyetemp->value() < 1E-6 )
    doubleSpinBox_debyetemp->setValue( calc.unitcell.debyeTemp );

  // plot the diagramm
  if( xAxisWavelength->isChecked() )
  {
    widget_plot_crosssections->graph(0)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdXsectCohEl ) );
    widget_plot_crosssections->graph(1)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdXsectCohInel ) );
    widget_plot_crosssections->graph(2)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdXsectIncEl ) );
    widget_plot_crosssections->graph(3)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdXsectIncInel ) );
    widget_plot_crosssections->graph(4)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdXsectAbs ) );
    widget_plot_crosssections->graph(5)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdXsectTotal ) );
    widget_plot_crosssections->graph(6)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdXsectSPh ) );
    widget_plot_crosssections->graph(7)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdXsectMPh ) );
    widget_plot_crosssections->legend->setVisible( true );

    widget_plot_attenuation->graph(0)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdAttenCoeff ) );
    widget_plot_attenuation->legend->setVisible( true );

    widget_plot_transmission->graph(0)->setData( QVector<double>::fromStdVector( calc.vdLambda ), QVector<double>::fromStdVector( calc.vdTransmission ) );
    widget_plot_transmission->legend->setVisible( true );
  }
  else if( xAxisEnergy->isChecked() )
  {
    QVector<double> vdEnergy( int(calc.vdLambda.size()) );
    for( unsigned int i=0; i<calc.vdLambda.size(); i++ )
    {
      vdEnergy[int(i)] = 8.18042531017E-2 / calc.vdLambda[i] / calc.vdLambda[i];
    }
    widget_plot_crosssections->graph(0)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdXsectCohEl ) );
    widget_plot_crosssections->graph(1)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdXsectCohInel ) );
    widget_plot_crosssections->graph(2)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdXsectIncEl ) );
    widget_plot_crosssections->graph(3)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdXsectIncInel ) );
    widget_plot_crosssections->graph(4)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdXsectAbs ) );
    widget_plot_crosssections->graph(5)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdXsectTotal ) );
    widget_plot_crosssections->graph(6)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdXsectSPh ) );
    widget_plot_crosssections->graph(7)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdXsectMPh ) );
    widget_plot_crosssections->legend->setVisible( true );

    widget_plot_attenuation->graph(0)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdAttenCoeff ) );
    widget_plot_attenuation->legend->setVisible( true );

    widget_plot_transmission->graph(0)->setData( vdEnergy, QVector<double>::fromStdVector( calc.vdTransmission ) );
    widget_plot_transmission->legend->setVisible( true );
  }

  if( action_auto_resize->isChecked() )
  {
    widget_plot_crosssections->rescaleAxes( true );
    widget_plot_attenuation->rescaleAxes( true );
    widget_plot_transmission->rescaleAxes( true );
  }
  widget_plot_crosssections->replot();
  widget_plot_attenuation->replot();
  widget_plot_transmission->replot();

  statusBar()->showMessage( "" );
  toolButton_plot->setEnabled( true );
  groupBox_diagram->setEnabled( true );

}


void MainWindow::onXAxisUnitTriggered( QAction *action )
{
  if( action == xAxisWavelength )
  {
    widget_plot_attenuation->xAxis->setLabel( "neutron wavelength [" + QString(QChar(0xC5)) + "]" );
    widget_plot_crosssections->xAxis->setLabel( "neutron wavelength [" + QString(QChar(0xC5)) + "]" );
    widget_plot_transmission->xAxis->setLabel( "neutron wavelength [" + QString(QChar(0xC5)) + "]" );

  }
  else if( action == xAxisEnergy )
  {
    widget_plot_attenuation->xAxis->setLabel( "neutron energy [eV]" );
    widget_plot_crosssections->xAxis->setLabel( "neutron energy [eV]" );
    widget_plot_transmission->xAxis->setLabel( "neutron energy [eV]" );
  }
//  onThreadFinished();
}


void MainWindow::onXAxisScalingTriggered(QAction *action)
{
  if( action == xAxisLinear )
  {
    widget_plot_attenuation->xAxis->setScaleType( QCPAxis::stLinear );
    widget_plot_crosssections->xAxis->setScaleType( QCPAxis::stLinear );
    widget_plot_transmission->xAxis->setScaleType( QCPAxis::stLinear );
  }
  else if( action == xAxisLog10 )
  {
    widget_plot_attenuation->xAxis->setScaleType( QCPAxis::stLogarithmic );
    widget_plot_attenuation->xAxis->grid()->setSubGridVisible(true);
    widget_plot_attenuation->xAxis->setScaleLogBase(10);
    widget_plot_attenuation->xAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
    widget_plot_attenuation->xAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    widget_plot_attenuation->xAxis->setSubTickCount(8);

    widget_plot_crosssections->xAxis->setScaleType(QCPAxis::stLogarithmic);
    widget_plot_crosssections->xAxis->grid()->setSubGridVisible(true);
    widget_plot_crosssections->xAxis->setScaleLogBase(10);
    widget_plot_crosssections->xAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
    widget_plot_crosssections->xAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    widget_plot_crosssections->xAxis->setSubTickCount(8);

    widget_plot_transmission->xAxis->setScaleType(QCPAxis::stLogarithmic);
    widget_plot_transmission->xAxis->grid()->setSubGridVisible(true);
    widget_plot_transmission->xAxis->setScaleLogBase(10);
    widget_plot_transmission->xAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
    widget_plot_transmission->xAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    widget_plot_transmission->xAxis->setSubTickCount(8);
  }
  widget_plot_attenuation->replot();
  widget_plot_crosssections->replot();
  widget_plot_transmission->replot();
}


void MainWindow::onYAxisScalingTriggered(QAction *action)
{
  if( action == yAxisLinear )
  {
    widget_plot_attenuation->yAxis->setScaleType( QCPAxis::stLinear );
    widget_plot_crosssections->yAxis->setScaleType( QCPAxis::stLinear );
    widget_plot_transmission->yAxis->setScaleType( QCPAxis::stLinear );
  }
  else if( action == yAxisLog10 )
  {
    widget_plot_attenuation->yAxis->setScaleType( QCPAxis::stLogarithmic );
    widget_plot_attenuation->yAxis->grid()->setSubGridVisible(true);
    widget_plot_attenuation->yAxis->setScaleLogBase(10);
    widget_plot_attenuation->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
    widget_plot_attenuation->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    widget_plot_attenuation->yAxis->setSubTickCount(8);

    widget_plot_crosssections->yAxis->setScaleType(QCPAxis::stLogarithmic);
    widget_plot_crosssections->yAxis->grid()->setSubGridVisible(true);
    widget_plot_crosssections->yAxis->setScaleLogBase(10);
    widget_plot_crosssections->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
    widget_plot_crosssections->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    widget_plot_crosssections->yAxis->setSubTickCount(8);

    widget_plot_transmission->yAxis->setScaleType(QCPAxis::stLogarithmic);
    widget_plot_transmission->yAxis->grid()->setSubGridVisible(true);
    widget_plot_transmission->yAxis->setScaleLogBase(10);
    widget_plot_transmission->yAxis->setNumberFormat("eb"); // e = exponential, b = beautiful decimal powers
    widget_plot_transmission->yAxis->setNumberPrecision(0); // makes sure "1*10^4" is displayed only as "10^4"
    widget_plot_transmission->yAxis->setSubTickCount(8);

  }
  widget_plot_attenuation->replot();
  widget_plot_crosssections->replot();
  widget_plot_transmission->replot();
}
