/*-------------------------------------------------*/
/* created by Mirko Boin                           */
/*            Helmholtz-Zentrum Berlin (HZB)       */
/*            boin@helmholtz-berlin.de             */
/*-------------------------------------------------*/
#ifndef BRAGGEDGECALCULATOR_H
#define BRAGGEDGECALCULATOR_H

#include <QObject>
#include <vector>
#include <nxs.h>


/**
  @author Mirko Boin, Helmholtz Centre Berlin for Materials and Energy <boin@helmholtz-berlin.de>
*/

/*! 
 *  \brief Container class for the cross section calculation.
 *
 */
class BraggEdgeCalculator : public QObject
{
  Q_OBJECT

  public:
    BraggEdgeCalculator();

    ~BraggEdgeCalculator();
    void setMarchDollaseTexture( std::vector<NXS_Texture> *textureVector );
    void setCalculationRange( double startLambda, double endLambda, unsigned int samplingPoints );
    void setSampleProperties(double packingdensity, double thickness_cm, double temperature );
    unsigned int nLambda;
    std::vector<double> vdLambda;
    std::vector<double> vdXsectCohEl;
    std::vector<double> vdXsectCohInel;
    std::vector<double> vdXsectIncEl;
    std::vector<double> vdXsectIncInel;
    std::vector<double> vdXsectAbs;
    std::vector<double> vdXsectTotal;
    std::vector<double> vdXsectSPh;
    std::vector<double> vdXsectMPh;
    std::vector<double> vdAttenCoeff;
    std::vector<double> vdTransmission;

    NXS_UnitCell unitcell;

public slots:
    void calculateCrosssections();

private:
  std::vector<NXS_Texture> *textureVector;
  double _samplePacking;
  double _sampleThickness;
  double _sampleTemperature;

};

#endif
