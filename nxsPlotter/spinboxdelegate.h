/*-------------------------------------------------*/
/* created by Mirko Boin                           */
/*            Helmholtz-Zentrum Berlin (HZB)       */
/*            boin@helmholtz-berlin.de             */
/*-------------------------------------------------*/
#ifndef SPINBOXDELEGATE_H
#define SPINBOXDELEGATE_H

#include <QItemDelegate>

class SpinBoxDelegate : public QItemDelegate
{
  Q_OBJECT

  public:
    SpinBoxDelegate(unsigned int maximum=15);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

  private:
    unsigned int maximum;

};


#endif // SPINBOXDELEGATE_H
