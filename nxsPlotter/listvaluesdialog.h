/*-------------------------------------------------*/
/* created by Mirko Boin                           */
/*            Helmholtz-Zentrum Berlin (HZB)       */
/*            boin@helmholtz-berlin.de             */
/*-------------------------------------------------*/
#ifndef LISTVALUESDIALOG_H
#define LISTVALUESDIALOG_H

#include <QDialog>
#include "ui_listvaluesdialog.h"

/**
  @author Mirko Boin, Helmholtz Centre Berlin for Materials and Energy <boin@helmholtz-berlin.de>
*/

/*! 
 *  \brief Dialog to list the plotted values.
 *
 */
class ListValuesDialog : public QDialog, private Ui::ListValuesDialog
{

  Q_OBJECT

  public:
    ListValuesDialog( QWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~ListValuesDialog();
    void setValuesText( QString );

private slots:
    void saveList();
};

#endif
