

#ifndef MARCHDOLLASEDIALOG_H
#define MARCHDOLLASEDIALOG_H

#include <QDialog>
#include "ui_marchdollasedialog.h"
#include <nxs.h>

class MarchDollaseDialog : public QDialog, private Ui::MarchDollaseDialog
{
  Q_OBJECT

  public:
    MarchDollaseDialog( QVector<Texture>* tv = 0, QWidget* parent = 0, Qt::WFlags fl = 0 );
    ~MarchDollaseDialog();
    QVector<Texture> &getTextureVector();
    /*$PUBLIC_FUNCTIONS$*/
  
  public slots:
    /*$PUBLIC_SLOTS$*/
  
  protected:
    /*$PROTECTED_FUNCTIONS$*/
  
  protected slots:
    /*$PROTECTED_SLOTS$*/
  
  private slots:
    void on_pushButton_addOrientation_clicked();
    void on_pushButton_delete_clicked();
    void on_listWidget_itemSelectionChanged();

  private:
    QVector<Texture>* textureVector;
};

#endif

