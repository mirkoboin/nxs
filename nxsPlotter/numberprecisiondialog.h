/*-------------------------------------------------*/
/* created by Mirko Boin                           */
/*            Helmholtz-Zentrum Berlin (HZB)       */
/*            boin@helmholtz-berlin.de             */
/*-------------------------------------------------*/
#ifndef NUMBERPRECISIONDIALOG_H
#define NUMBERPRECISIONDIALOG_H

#include <QDialog>
#include "ui_numberprecisiondialog.h"


/**
  @author Mirko Boin, Helmholtz Centre Berlin for Materials and Energy <boin@helmholtz-berlin.de>
*/

/*! 
 *  \brief Dialog to set up the number precision (decimal places).
 *
 */
class NumberPrecisionDialog : public QDialog, private Ui::NumberPrecisionDialog
{
  Q_OBJECT

  public:
    NumberPrecisionDialog( QWidget* parent = nullptr, Qt::WindowFlags fl = nullptr );
    ~NumberPrecisionDialog();
    unsigned int getNumberPrecision();
    void setNumberPrecision(int prec);
    double getStartWavelength();
    void setStartWavelength(double lambda);
    double getStopWavelength();
    void setStopWavelength(double lambda);
    unsigned int getSamplingPoints();
    void setSamplingPoints(unsigned int points);
    unsigned int getMaxHKL();
    void setMaxHKL(unsigned int maxHKL);

  public slots:
    /*$PUBLIC_SLOTS$*/

  protected:
    /*$PROTECTED_FUNCTIONS$*/

  private slots:
    void onDecimalPlacesValueChanged(int prec);

};

#endif

