#-------------------------------------------------
# created by Mirko Boin
#            Helmholtz-Zentrum Berlin (HZB)
#            boin@helmholtz-berlin.de
#-------------------------------------------------

include( ../build.pri )

NXSPLOTTER_VER_MAJ = 1
NXSPLOTTER_VER_MIN = 3
NXSPLOTTER_VERSION = '\\"$${NXSPLOTTER_VER_MAJ}.$${NXSPLOTTER_VER_MIN}\\"'
DEFINES += VER=$${NXSPLOTTER_VERSION}
DEFINES += SVN_REVISION=\"'\\"$(shell svnversion -n .)\\"'\"
TEMPLATE = app

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG(release, debug|release) {
    DESTDIR = Release
} else {
    DESTDIR = Debug
}

OBJECTS_DIR = $$DESTDIR
MOC_DIR = $$DESTDIR
RCC_DIR = $$DESTDIR
UI_DIR = $$DESTDIR

RC_FILE = app.rc

FORMS += mainwindow.ui \
    listvaluesdialog.ui \
    numberprecisiondialog.ui \
    plotcolorsdialog.ui \
    inputfiledialog.ui \
    aboutdialog.ui
SOURCES += main.cpp \
    mainwindow.cpp \
    braggedgecalculator.cpp \
    listvaluesdialog.cpp \
    numberprecisiondialog.cpp \
    plotcolorsdialog.cpp \
    colorbox.cpp \
    inputfiledialog.cpp \
    doublespinboxdelegate.cpp \
    spinboxdelegate.cpp \
    aboutdialog.cpp \
    qcustomplot.cpp
HEADERS += mainwindow.h \
    braggedgecalculator.h \
    listvaluesdialog.h \
    numberprecisiondialog.h \
    plotcolorsdialog.h \
    colorbox.h \
    inputfiledialog.h \
    doublespinboxdelegate.h \
    spinboxdelegate.h \
    aboutdialog.h \
    qcustomplot.h
RESOURCES += resource.qrc



debug {
  NXSLIBDIR = ../libnxs/Debug/
}
release {
  NXSLIBDIR = ../libnxs/Release/
}

INCLUDEPATH += ../libnxs ./

LIBS += -L$${NXSLIBDIR} -lnxs

DISTFILES += \
    examples.txt

win32 {
    DEPLOY_COMMAND = windeployqt
    DEPLOY_ARGS += --compiler-runtime \
        --release \
        --no-translations \
        --no-svg \
        --dir \"$${target.path}\" \
        --printsupport
    DEPLOY_TARGET = $${DESTDIR}/$${TARGET}.exe

    mydeployment.commands = $${DEPLOY_COMMAND} $${DEPLOY_ARGS} $${DEPLOY_TARGET}
    mydeployment.path = $$target.path
    INSTALLS += mydeployment
}

INSTALLS += target
