


#include "plotcolorsdialog.h"

PlotColorsDialog::PlotColorsDialog(QWidget* parent, Qt::WindowFlags fl)
: QDialog( parent, fl ), Ui::PlotColorsDialog()
{
	setupUi(this);
}

PlotColorsDialog::~PlotColorsDialog()
{
}

/*$SPECIALIZATION$*/




/*!
    \fn PlotColorsDialog::setColors( QColor, QColor, QColor, QColor, QColor, QColor )
 */
void PlotColorsDialog::setColors( QColor coh_el, QColor coh_inel, QColor inc_el, QColor inc_inel, QColor absorption, QColor total )
{
  box_coh_el->setColor( coh_el );
  box_coh_inel->setColor( coh_inel );
  box_inc_el->setColor( inc_el );
  box_inc_inel->setColor( inc_inel );
  box_absorption->setColor( absorption );
  box_total->setColor( total );
}



/*!
    \fn PlotColorsDialog::color_coh_el()
 */
QColor PlotColorsDialog::color_coh_el()
{
  return box_coh_el->color();
}


/*!
    \fn PlotColorsDialog::color_coh_inel()
 */
QColor PlotColorsDialog::color_coh_inel()
{
  return box_coh_inel->color();
}


/*!
    \fn PlotColorsDialog::color_inc_el()
 */
QColor PlotColorsDialog::color_inc_el()
{
  return box_inc_el->color();
}


/*!
    \fn PlotColorsDialog::color_inc_inel()
 */
QColor PlotColorsDialog::color_inc_inel()
{
  return box_inc_inel->color();
}


/*!
    \fn PlotColorsDialog::color_absorption()
 */
QColor PlotColorsDialog::color_absorption()
{
  return box_absorption->color();
}


/*!
    \fn PlotColorsDialog::color_total()
 */
QColor PlotColorsDialog::color_total()
{
  return box_total->color();
}


