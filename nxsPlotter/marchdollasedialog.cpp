


#include "marchdollasedialog.h"

MarchDollaseDialog::MarchDollaseDialog(QVector<Texture>* tv, QWidget* parent, Qt::WFlags fl)
: QDialog( parent, fl ), Ui::MarchDollaseDialog()
{
	setupUi(this);
  if( tv )
  {
    for( int i=0; i<tv->size(); i++ )
    {
      QString text;
      text.sprintf( "h=%i  k=%i  l=%i  r=%.2f  f=%.2f", tv->at(i).a, tv->at(i).b, tv->at(i).c, tv->at(i).r, tv->at(i).f );
      listWidget->addItem( text );
    }
  }

  textureVector = new QVector<Texture>( *tv );
}

MarchDollaseDialog::~MarchDollaseDialog()
{
}

/*$SPECIALIZATION$*/




/*!
    \fn MarchDollaseDialog::on_pushButton_addOrientation_clicked()
 */
void MarchDollaseDialog::on_pushButton_addOrientation_clicked()
{
  QString text;
  text.sprintf( "h=%i  k=%i  l=%i  r=%.2f  f=%.2f", spinBox_h->value(), spinBox_k->value(), spinBox_l->value(), doubleSpinBox_r->value(), doubleSpinBox_f->value() );
  listWidget->addItem( text );

  Texture tex;
  tex.a = spinBox_h->value();
  tex.b = spinBox_k->value();
  tex.c = spinBox_l->value();
  tex.r = doubleSpinBox_r->value();
  tex.f = doubleSpinBox_f->value();
  textureVector->append( tex );
}


/*!
    \fn MarchDollaseDialog::on_pushButton_delete_clicked()
 */
void MarchDollaseDialog::on_pushButton_delete_clicked()
{
  int row = listWidget->currentRow();

  if( row != -1 )
  {
    delete( listWidget->takeItem( row ) );
    textureVector->remove( row );
  }
}


/*!
    \fn MarchDollaseDialog::getTextureVector()
 */
QVector<Texture> & MarchDollaseDialog::getTextureVector()
{
  return *textureVector;
}



/*!
    \fn MarchDollaseDialog::on_listWidget_itemSelectionChanged()
 */
void MarchDollaseDialog::on_listWidget_itemSelectionChanged()
{
//  int row = listWidget->currentRow();
  QString text = listWidget->currentItem()->text();
  QRegExp rx("^h=(.*)  k=(.*)  l=(.*)  r=(.*)  f=(.*)$");
  if( rx.exactMatch(text) )
  {
    spinBox_h->setValue( rx.cap(1).toInt() );
    spinBox_k->setValue( rx.cap(2).toInt() );
    spinBox_l->setValue( rx.cap(3).toInt() );
    doubleSpinBox_r->setValue( rx.cap(4).toDouble() );
    doubleSpinBox_f->setValue( rx.cap(5).toDouble() );
  }

}
