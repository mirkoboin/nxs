


#include "inputfiledialog.h"
#include <QFileDialog>

InputFileDialog::InputFileDialog(QWidget* parent, Qt::WindowFlags fl)
: QDialog( parent, fl ), Ui::InputFileDialog()
{
	setupUi(this);
}

InputFileDialog::~InputFileDialog()
{
}

/*$SPECIALIZATION$*/




/*!
    \fn InputFileDialog::setFileName( QString )
 */
void InputFileDialog::setFileName( QString fileName )
{
  lineEdit_fileName->setText( fileName );
}



/*!
    \fn InputFileDialog::getFileName()
 */
QString InputFileDialog::getFileName()
{
  return lineEdit_fileName->text();
}


/*!
    \fn InputFileDialog::on_pushButton_open_clicked()
 */
void InputFileDialog::on_pushButton_open_clicked()
{
  QString fileName = QFileDialog::getOpenFileName( this, tr("Open file"), lineEdit_fileName->text(), tr("All files (*.*)"));

  if ( !fileName.isEmpty() )
  {
    lineEdit_fileName->setText( fileName );
  }
}
