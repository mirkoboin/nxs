#include "aboutdialog.h"
#include "ui_aboutdialog.h"
#include <nxs.h>

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    QString versiontext;
  #ifdef VER
  #define VERNUM2STR(x) VERSTR(x)
  #define VERSTR(x) #x
    QString version(VERNUM2STR( VER ));
    if( !version.isEmpty() )
      versiontext.append( "v" + version.simplified().remove('"') );
  #ifdef SVN_REVISION
    QString revision(SVN_REVISION);
    if( !revision.isEmpty() )
    {
      revision = revision.simplified();
      QRegExp pattern( "\\d*:?(\\d+)\\D*" );
      if( pattern.indexIn(revision) > -1 )
        versiontext.append( "-r" + pattern.cap(1) );
    }
  #endif
  #endif
    ui->label_version->setText( versiontext );

    ui->label_libnxs->setText( ui->label_libnxs->text().append( QString(nxs_version()) ) );
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
