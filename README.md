# nxs - a library for neutron cross section calculations #



The nxs library for computing neutron scattering and absorption cross sections provides a number of C structs and functions to calculate wavelength-dependent cross section values for polycrystalline/powder-like materials. The definition of a material is represented by the composition of a unit cell (NXS_UnitCell). A unit cell is created from the specification of a space group and its unit cell parameters. The SgInfo routines from Ralf W. Grosse-Kunstleve is included here for such purposes. Monoatomic materials as well as multi-atomic compounds are created by adding atom information/properties (NXS_AtomInfo). The library also provides a reading and saving routines to compose unit cells from nxs parameter files. Go ahead and try!

Use git to stay up-to-date: 
git clone https://bitbucket.org/mirkoboin/nxs

Further information on the wiki!

Author: Mirko Boin, boin@helmholtz-berlin.de