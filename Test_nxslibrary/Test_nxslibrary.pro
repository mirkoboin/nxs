SOURCES += \
    main.c

TEMPLATE = app
CONFIG += console
QT -= gui

debug {
  NXSLIBDIR = ../libnxs/Debug/
}
release {
  NXSLIBDIR = ../libnxs/Release/
}

PRE_TARGETDEPS += $${NXSLIBDIR}/libnxs.a


INCLUDEPATH += ../libnxs ./

LIBS += -L$${NXSLIBDIR} -lnxs
