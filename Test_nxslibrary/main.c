
#include <nxs.h>
#include <stdio.h>

int main( int argc, char* argv[] )
{
  NXS_UnitCell uc,uc2;
  NXS_AtomInfo *atomInfoList = NULL;
  double coh = 0.0;

//  std::string filename = "Al.nxs";
//  NXS_UnitCell uc;
//  NXS_AtomInfo *atomInfoList;
//  int numAtomInfos = nxs_readParameterFile( filename.c_str(), &uc, &atomInfoList);
//  if (numAtomInfos<=0) {
//    printf("ERROR: Could not read crystal information from file \"%s\"\n",filename.c_str());
//  }
  unsigned int i = 0;

  unsigned int nAI = nxs_readParameterFile("../libnxs/nxs-files/Fe_gamma.nxs", &uc, &atomInfoList);
  printf("%i\n", nAI);

  if( nAI>=0 )
    printf("NXS unit cell definition is:\n"
          "space group number = %s\n"
          "a = %.5f \t\t alpha = %.5f\n"
          "b = %.5f \t\t beta  = %.5f\n"
          "c = %.5f \t\t gamma = %.5f\n"
          "# label  b_coherent  sigma_inc  sigma_abs  molar_mass  debye_temp  x  y  z\n",
          uc.spaceGroup, uc.a, uc.alpha, uc.b, uc.beta, uc.c, uc.gamma);

  if( NXS_ERROR_OK != nxs_initUnitCell(&uc) )
  {
    fprintf(stderr, "WARNING: nxs_initUnitCell() failed\n");
  }
  else
  {
    uc.temperature = 293.0;

    for(i=0; i<nAI; i++)
    {
      nxs_addAtomInfo( &uc, atomInfoList[i] );
      printf("%s  %f  %f  %f  %f  %f  %f  %f  %f\n",
           atomInfoList[i].label, atomInfoList[i].b_coherent, atomInfoList[i].sigmaIncoherent,
           atomInfoList[i].sigmaAbsorption, atomInfoList[i].molarMass, atomInfoList[i].siteOccupation,
           atomInfoList[i].x[0], atomInfoList[i].y[0], atomInfoList[i].z[0] );
    }
  }

  uc.maxHKL_index = 15;
  nxs_initHKL( &uc );

  for(i=0; i<uc.hklList[0].multiplicity/2; i++)
  {
    printf("%i %i %i\n", uc.hklList[0].equivHKL[i].h, uc.hklList[0].equivHKL[i].k, uc.hklList[0].equivHKL[i].l );
  }
  printf("%i\n", uc.hklList[0].multiplicity/2);


  nAI = nxs_initFromParameterFile( "../libnxs/nxs-files/AISI_304L.nxs", &uc2 );
  printf("NXS unit cell definition is:\n"
          "space group number = %s\n"
          "a = %.5f \t\t alpha = %.5f\n"
          "b = %.5f \t\t beta  = %.5f\n"
          "c = %.5f \t\t gamma = %.5f\n"
          "debye_temp = %.5f\n"
          "# label  b_coherent  sigma_inc  sigma_abs  molar_mass  sof   x  y  z\n",
          uc2.spaceGroup, uc2.a, uc2.alpha, uc2.b, uc2.beta, uc2.c, uc2.gamma, uc2.debyeTemp);

  for(i=0; i<uc2.nAtomInfo; i++)
  {
    NXS_AtomInfo ai = uc2.atomInfoList[i];
    printf("%s  %f  %f  %f  %f  %f  %f  %f  %f\n",
         ai.label, ai.b_coherent, ai.sigmaIncoherent,
         ai.sigmaAbsorption, ai.molarMass, ai.siteOccupation,
         ai.x[0], ai.y[0], ai.z[0] );
  }

  for(i=0; i<uc2.nAtomInfo; i++)
  {
    NXS_AtomInfo ai = uc2.atomInfoList[i];
    printf("%s  %f  %f  %i  %i\n", ai.label, ai.b_coherent, ai.siteOccupation, ai.nAtoms, uc2.nAtoms );
    coh += ai.b_coherent * ai.siteOccupation;
  }
  printf("abs at 4AA = %.3f\n", nxs_Absorption( 4.0, &uc) );

  printf("abs at 4AA = %.3f\n", nxs_Absorption( 4.0, &uc2) );


//  nxs_saveParameterFile("Al_copy.nxs", &uc);

  return 0;
}

